# pylint: disable=invalid-name
# pylint: disable=protected-access
# pylint: disable=wildcard-import
# pylint: disable=unused-wildcard-import
import json
from collections import defaultdict
from datetime import datetime
from os import path
from pathlib import Path
from typing import List, Sequence

import pytest
import pytest_html
import structlog
from _pytest import config as pytest_config
from _pytest import python as pytest_python
from _pytest.config import argparsing as pytest_argparsing
from cucumber_tag_expressions import parse
from pytest_bdd import parser as pytest_bdd_parser
from pytest_html import extras
from pytest_selenium import pytest_selenium
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.remote.webdriver import WebDriver
from browserstack.local import Local

# import all api step defs
from common.api.step_definitions.steps_auth import *
from common.api.step_definitions.steps_requests import *
from common.api.step_definitions.steps_verify_response import *
from common.ui.page_objects.selenium_generics import SeleniumGenerics

# import all ui step defs
from common.ui.step_definitions.steps_custom import *
from common.ui.step_definitions.steps_given import *
from common.ui.step_definitions.steps_then import *
from common.ui.step_definitions.steps_when import *
from lib.custom_commands import my_custom_commands

# do not remove this unused import. this sets up pytest short summary.
from lib.pytest_terminal_report import pytest_terminal_summary

# do not remove this unused import. this sets up logging.
from utils import log_handler
from utils.browser_actions import BrowserActions
from utils.env_variables import EnvVariables
from utils.locators import Locators
from utils.utils import initialize_output_dirs

logger = structlog.get_logger(__name__)

browser_configs = []
test_ids: List[str] = []
pytest_plugins: Sequence[str] = ("lib.pytest_testrail_client.pytest_testrail_client",)


def pytest_configure(config: pytest_config.Config) -> None:
    """Allow plugins and conftest files to perform initial configuration.

    This hook is called for every plugin and initial conftest file
    after command line options have been parsed.

    After that, the hook is called for other conftest files as they are
    imported.

    .. note::
        This hook is incompatible with ``hookwrapper=True``.

    :param _pytest.config.Config config: The pytest config object.

    Reference for docstring:
    https://docs.pytest.org/en/6.2.x/_modules/_pytest/hookspec.html#pytest_configure
    """
    config.option.keyword = "automated"
    config.option.markexpr = "not not_in_scope"
    mat_option = config.getoption("--device-matrix", default=None)
    if mat_option:
        with open(str(mat_option), "r") as mat:
            matrix = json.load(mat)
            browser_configs.extend(matrix)


def pytest_addoption(parser: pytest_argparsing.Parser) -> None:
    """Register argparse-style options and ini-style config values,
    called once at the beginning of a test run.

    .. note::

        This function should be implemented only in plugins or ``conftest.py``
        files situated at the tests root directory due to how pytest
        :ref:`discovers plugins during startup <pluginorder>`.

    :param _pytest.config.argparsing.Parser parser:
        To add command line options, call
        :py:func:`parser.addoption(...) <_pytest.config.argparsing.Parser.addoption>`.
        To add ini-file values call :py:func:`parser.addini(...)
        <_pytest.config.argparsing.Parser.addini>`.

    :param _pytest.config.PytestPluginManager pluginmanager:
        pytest plugin manager, which can be used to install :py:func:`hookspec`'s
        or :py:func:`hookimpl`'s and allow one plugin to call another plugin's hooks
        to change how command line options are added.

    Options can later be accessed through the
    :py:class:`config <_pytest.config.Config>` object, respectively:

    - :py:func:`config.getoption(name) <_pytest.config.Config.getoption>` to
      retrieve the value of a command line option.

    - :py:func:`config.getini(name) <_pytest.config.Config.getini>` to retrieve
      a value read from an ini-style file.

    The config object is passed around on many internal objects via the ``.config``
    attribute or can be retrieved as the ``pytestconfig`` fixture.

    .. note::
        This hook is incompatible with ``hookwrapper=True``.

    Reference for docstring:
    https://docs.pytest.org/en/6.2.x/_modules/_pytest/hookspec.html#pytest_addoption
    """
    parser.addoption(
        "--language",
        action="store",
        default="en",
        type=str,
        help="Application language",
    )
    parser.addoption(
        "--proxy-url",
        metavar="str",
        help="The proxy to be used by any network request - browser or otherwise",
    )
    parser.addoption(
        "--locators",
        metavar="str",
        help="The path to a json file or a folder of json files containing the locators needed for the test(s)",
    )
    parser.addoption("--tags", metavar="str", help="Will filter tests by given tags")
    parser.addoption(
        "--device-matrix",
        metavar="str",
        help="The different configurations against which the test would be run",
    )
    parser.addoption(
        "--env-config",
        metavar="Configuration",
        type=str,
        default=None,
        help="Provide Environment Configuration class name (keyname as per config/envconf.json).",
    )


def pytest_collection_modifyitems(
    config: pytest_config.Config, items: List[pytest.Item]
) -> None:
    """Called after collection has been performed. May filter or re-order
    the items in-place.

    :param pytest.Session session: The pytest session object.
    :param _pytest.config.Config config: The pytest config object.
    :param List[pytest.Item] items: List of item objects.

    Reference for docstring:
    https://docs.pytest.org/en/6.2.x/_modules/_pytest/hookspec.html#pytest_collection_modifyitems
    """
    if (
        "pytest_testrail_export_test_cases" not in config.option
        or config.option.pytest_testrail_export_test_cases is False
    ):
        raw_tags = config.option.tags
        if raw_tags is not None:
            for item in items:
                item_tags = [marker.name for marker in item.own_markers]
                if not parse(raw_tags).evaluate(item_tags):
                    item.add_marker(pytest.mark.not_in_scope)


def pytest_sessionstart(session: pytest.Session) -> None:
    """Called after the ``Session`` object has been created and before performing collection
    and entering the run test loop.

    :param pytest.Session session: The pytest session object.

    Reference for docstring:
    https://docs.pytest.org/en/6.2.x/_modules/_pytest/hookspec.html#pytest_sessionstart
    """
    initialize_output_dirs()
    if browser_configs:
        # delete and overwrite the locally defined selenium fixture if we have configs from file
        global selenium
        del selenium

        @pytest.fixture
        def selenium(multi_browser):
            return multi_browser


def pytest_bdd_before_scenario(
    request: FixtureRequest,
    feature: pytest_bdd_parser.Feature,
    scenario: pytest_bdd_parser.Scenario,
):
    """Called before every scenario execution"""
    request.node.scenarioDict = defaultdict()
    logger.info(
        "Scenario Execution Started.", scenario_name=scenario.name, feature=feature.name
    )


def pytest_bdd_after_scenario(
    feature: pytest_bdd_parser.Feature, scenario: pytest_bdd_parser.Scenario
):
    """Called after every scenario is executed"""
    logger.info(
        "Scenario Execution Completed.",
        scenario_name=scenario.name,
        feature=feature.name,
    )


def pytest_bdd_after_step(
    feature: pytest_bdd_parser.Feature,
    scenario: pytest_bdd_parser.Scenario,
    step: pytest_bdd_parser.Step,
):
    """Called after every step execution"""
    logger.info(
        "Step Passed.",
        feature=feature.name,
        scenario=scenario.name,
        step=step.name,
    )


def pytest_generate_tests(metafunc: pytest_python.Metafunc) -> None:
    """Generate (multiple) parametrized calls to a test function.

    Reference for docstring:
    https://docs.pytest.org/en/6.2.x/_modules/_pytest/hookspec.html#pytest_generate_tests
    """
    if browser_configs:
        if not test_ids:
            # Generate test IDs from the configs
            for config in browser_configs:
                browser_name = config.get("browser", "").strip().lower()
                browser_version = config.get("browser_version", "").strip().lower()
                resolution = config.get("resolution", "").strip().lower()
                device = config.get("device", "").strip().lower()
                real_device = ""
                if device:
                    real_device = (
                        "realDevice"
                        if device
                        and config.get("real_mobile", "").strip().lower() == "true"
                        else "emulated"
                    )
                os = config.get("os", "").strip().lower()
                os_version = config.get("os_version", "").strip().lower()
                orientation = config.get("deviceOrientation", "").strip().lower()

                field_list: List[str] = [
                    device,
                    os,
                    os_version,
                    real_device,
                    browser_name,
                    browser_version,
                    resolution,
                    orientation,
                ]
                id_str = "-".join([field for field in field_list if field])

                test_ids.append(id_str)

        metafunc.parametrize(
            "custom_browser_config", browser_configs, ids=test_ids, indirect=True
        )


def _chrome_specific_setup(driver_class, browser_config, proxy_url) -> WebDriver:
    from selenium.webdriver.chrome.options import Options

    options = Options()
    # check if we have a value for headless. default is false
    if browser_config.get("headless", "false").strip().lower() != "false":
        options.add_argument("--headless")

    if proxy_url:
        options.add_argument("--ignore-certificate-errors")
        options.add_argument(f"--proxy-server={proxy_url}")

    return driver_class(options=options, executable_path=browser_config["driver_path"])


def _edge_specific_setup(driver_class, browser_config, proxy_url) -> WebDriver:
    return driver_class(
        capabilities={"use_chromium": True, "headless": True},
        executable_path=browser_config["driver_path"],
    )


def _firefox_specific_setup(driver_class, browser_config, proxy_url) -> WebDriver:
    from selenium.webdriver.firefox.options import Options

    options = Options()
    # check if we have a value for headless. default is false
    if browser_config.get("headless", "false").strip().lower() != "false":
        options.add_argument("-headless")

    firefox_capabilities = DesiredCapabilities.FIREFOX
    if proxy_url:
        firefox_capabilities["marionette"] = True

        firefox_capabilities["proxy"] = {
            "proxyType": "MANUAL",
            "httpProxy": proxy_url,
            "ftpProxy": proxy_url,
            "sslProxy": proxy_url,
        }
        firefox_capabilities["acceptInsecureCerts"] = True
    return driver_class(
        options=options,
        executable_path=browser_config["driver_path"],
        capabilities=firefox_capabilities,
    )


def _browser_specific_setup(driver_class, browser_config, proxy_url) -> WebDriver:
    if driver_class == pytest_selenium.SUPPORTED_DRIVERS["chrome"]:
        return _chrome_specific_setup(driver_class, browser_config, proxy_url)
    elif driver_class == pytest_selenium.SUPPORTED_DRIVERS["edge"]:
        return _edge_specific_setup(driver_class, browser_config, proxy_url)
    elif driver_class == pytest_selenium.SUPPORTED_DRIVERS["firefox"]:
        return _firefox_specific_setup(driver_class, browser_config, proxy_url)

    logger.warning("Browser not recognized")
    return None


def _driver_common_after_setup(driver, config, env_variables):
    resolution_str = config.get(
        "resolution", env_variables.get("DEFAULT_RESOLUTION", "1366x768")
    )
    resolution = [int(num_str.strip()) for num_str in resolution_str.split("x")]
    driver.set_window_size(resolution[0], resolution[1])


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    # execute all other hooks to obtain the report object
    outcome = yield
    rep = outcome.get_result()

    # set a report attribute for each phase of a call, which can
    # be "setup", "call", "teardown"

    setattr(item, "rep_" + rep.when, rep)

    extra = getattr(rep, "extra", [])
    if rep.when == "call":
        feature_request = item.funcargs["request"]
        if feature_request.config.option.device_matrix is not None:
            driver = feature_request.getfixturevalue("multi_browser")
            xfail = hasattr(rep, "wasxfail")
            if (rep.skipped and xfail) or (rep.failed and not xfail):
                # only add additional html on failure
                ss_file = f"output/screenshots/{feature_request.node.name}.png"
                driver.save_screenshot(ss_file)
                extra.append(pytest_html.extras.image(ss_file))
            rep.extra = extra


@pytest.fixture
def multi_browser(
    custom_browser_config, env_variables: EnvVariables, proxy_url, request: FixtureRequest
) -> WebDriver:
    bs_local = None
    driver = None
    if custom_browser_config.get("local", "").strip().lower() == "true":
        browser_given = custom_browser_config["browser"]
        driver_class = pytest_selenium.SUPPORTED_DRIVERS[browser_given]
        driver = _browser_specific_setup(driver_class, custom_browser_config, proxy_url)
        _driver_common_after_setup(driver, custom_browser_config, env_variables)
    else:
        if env_variables.get("CI") == "false":
            bs_local = Local()
            bs_local_args = {"key": env_variables.get('BROWSERSTACK_ACCESS_KEY'), "force": "true", "forcelocal": "true", "local-proxy-host": env_variables.get("HOST"), "local-proxy-port": env_variables.get("PORT"), "localIdentifier": custom_browser_config["browserstack.localIdentifier"]}
            bs_local.start(**bs_local_args)
        build_name = custom_browser_config["project"] + "-" + custom_browser_config["browser"] + "-" + datetime.now().strftime('%Y%m%d-%H%M%S')
        custom_browser_config["build"] = build_name
        driver_class = pytest_selenium.SUPPORTED_DRIVERS["BrowserStack"]
        driver = driver_class(
            command_executor=f"https://{env_variables.get('BROWSERSTACK_USER')}:{env_variables.get('BROWSERSTACK_ACCESS_KEY')}@hub-cloud.browserstack.com/wd/hub",
            desired_capabilities=custom_browser_config
        )
        driver.execute_script(
            'browserstack_executor: {"action": "setSessionName", "arguments": {"name": "' + request.node.name + '"}}')

    driver.maximize_window()
    yield driver

    if not custom_browser_config.get("local", "").strip().lower() == "true":
        if request.node.rep_setup.passed and request.node.rep_call.passed:
            driver.execute_script(
                'browserstack_executor: {"action": "setSessionName", "arguments": {"status": "PASSED"}}')
        elif request.node.rep_setup.failed or request.node.rep_call.failed:
            driver.execute_script(
                'browserstack_executor: {"action": "setSessionName", "arguments": {"status": "FAILED"}}')

    driver.quit()
    if bs_local:
        bs_local.stop()


@pytest.fixture
def selenium(selenium, selenium_patcher, variables):
    my_custom_commands()
    selenium.delete_all_cookies()
    return selenium


@pytest.fixture
def selenium_generics(selenium) -> SeleniumGenerics:
    return SeleniumGenerics(selenium)


@pytest.fixture
def browser(selenium) -> BrowserActions:
    """Fixture to setup BrowserActions"""
    return BrowserActions(selenium)


@pytest.fixture(scope="session")
def proxy_url(request):
    proxy_url_value = request.config.getoption("--proxy-url")
    return proxy_url_value if proxy_url_value else None


@pytest.fixture
def chrome_options(chrome_options, variables, proxy_url, request):
    caps: dict = {k: v for k, v in request.config.getoption("--capability")}
    if (caps.get("headless", "") == "True") or (
        "capabilities" in variables
        and "headless" in variables["capabilities"]
        and variables["capabilities"]["headless"] == "True"
    ):
        chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument("--window-size=1367,768")
    if proxy_url:
        chrome_options.add_argument("--ignore-certificate-errors")
        chrome_options.add_argument(f"--proxy-server={proxy_url}")

    return chrome_options


@pytest.fixture
def firefox_options(firefox_options):
    firefox_options.add_argument('-headless')
    firefox_options.add_argument('-foreground')
    return firefox_options


@pytest.fixture(scope="session")
def session_capabilities(proxy_url, session_capabilities: dict):
    if proxy_url:
        proxy_caps: dict = {
            "proxyType": "MANUAL",
            "httpProxy": proxy_url,
            "sslProxy": proxy_url,
        }

        # merging the dictionaries
        return {**session_capabilities, **proxy_caps}

    return session_capabilities


@pytest.fixture
def capabilities(capabilities):
    if "browser" in capabilities and capabilities["browser"] in [
        "Edge",
        "MicrosoftEdge",
    ]:
        capabilities["browserstack.edge.enablePopups"] = "true"
    if "browser" in capabilities and capabilities["browser"] in ["safari", "Safari"]:
        capabilities["browserstack.safari.enablePopups"] = "true"
    return capabilities


# needs to be session scoped with auto use enabled for this to show up
# in the list of fixtures in the generate tests hook
@pytest.fixture(scope="session", autouse=True)
def custom_browser_config(request):
    try:
        return request.param
    except:
        return None


@pytest.fixture(scope="session")
def language(request):
    language_value = request.config.getoption("language")
    return language_value if language_value else None


@pytest.fixture(scope="session")
def project_dir(request, pytestconfig) -> str:
    path_str = request.session.config.known_args_namespace.confcutdir
    # the value above is None in some cases(not sure why). so, adding the fallback below
    return path_str if path_str else str(pytestconfig.rootdir)


@pytest.fixture(scope="session", autouse=True)
def locators(request, project_dir) -> Locators:
    path_str = request.config.getoption("--locators")
    # Default is the project root if no value is specified explicitly
    path_str = path_str if path_str else project_dir

    import pathlib

    locators_path = pathlib.Path(path_str)
    if not locators_path.exists():
        raise Exception(f"Locator file/folder '{path_str}' not found. Exiting")

    json_files = []
    if locators_path.is_dir():
        # recursively traverse the dir and get all files with the specified pattern
        patterns = ["*locator.json", "*locators.json", "locator*.json"]
        for pattern in patterns:
            json_files.extend(list(locators_path.rglob(pattern)))
        logger.debug("List of Locator json files", files=json_files)
        if not json_files:
            raise Exception(
                f"Folder '{path_str}' does not contain any valid locator files. Exiting"
            )
    else:
        json_files = [str(locators_path)]

    return Locators(json_files)


@pytest.fixture(scope="session")
def env_variables(request):
    envconfig = request.config.getoption("--env-config")
    return EnvVariables(env_config=envconfig)


@pytest.fixture(scope="session")
def base_url(request, env_variables) -> str:
    # get base url value from command line
    # if it returns nothing, then look for environment variables and return.
    return request.config.getoption("--base-url") or env_variables.get(
        "BASE_URL", default=""
    )


@pytest.fixture(scope="session")
def api_response_container() -> dict:
    """api_response_container

    Fixture to initialize an empty dictionary to store api responses
    """
    return dict()
