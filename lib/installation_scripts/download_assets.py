import json
import os
import platform
import tarfile

import requests

BROWSERSTACK_LOCAL_MAC = (
    "https://www.browserstack.com/browserstack-local/BrowserStackLocal-darwin-x64.zip"
)
BROWSERSTACK_LOCAL_LINUX = (
    "https://www.browserstack.com/browserstack-local/BrowserStackLocal-linux-x64.zip"
)
BROWSERSTACK_LOCAL_WINDOWS = (
    "https://www.browserstack.com/browserstack-local/BrowserStackLocal-win32.zip"
)

CHROMEDRIVER_MAC = (
    "https://chromedriver.storage.googleapis.com/%s/chromedriver_mac64.zip"
)
CHROMEDRIVER_LINUX = (
    "https://chromedriver.storage.googleapis.com/%s/chromedriver_linux64.zip"
)
CHROMEDRIVER_WINDOWS = (
    "https://chromedriver.storage.googleapis.com/%s/chromedriver_win32.zip"
)

GECKO_DRIVER_LATEST_URL = (
    "https://api.github.com/repos/mozilla/geckodriver/releases/latest"
)
GECKO_DRIVER_URL = (
    "https://github.com/mozilla/geckodriver/releases/download/%s/geckodriver-%s"
)
GECKODRIVER_MAC = GECKO_DRIVER_URL + "-macos.tar.gz"
GECKODRIVER_LINUX = GECKO_DRIVER_URL + "-linux32.tar.gz"
GECKODRIVER_WINDOWS = GECKO_DRIVER_URL + "-win64.zip"


def _unzip(zip_file, unzip_dir):
    stream = os.popen("unzip -o %s -d %s" % (zip_file, unzip_dir))
    output = stream.read()
    print(output)


def _extract_tarball(tar_file, out_dir):
    tf = tarfile.open(tar_file)
    tf.extractall(out_dir)
    tf.close()


def _create_dir_if_needed(dir_path):
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)


def _download_file(url, dest_path):
    response = requests.get(url, allow_redirects=True)
    with open(dest_path, "wb") as f:
        f.write(response.content)


def _get_platform_name():
    return platform.system().lower()


def _download_platform_specific_file(
    name, temp_dir, dest_dir, platform_url_map, platform_filename_map
):
    platform_type = _get_platform_name()
    print(f"Downloading {name} for {platform_type} platform")

    _create_dir_if_needed(temp_dir)

    download_filename = platform_filename_map[platform_type]
    download_path = temp_dir + f"/{download_filename}"
    _download_file(platform_url_map[platform_type], download_path)

    _create_dir_if_needed(dest_dir)
    if "tar.gz" in download_filename:
        _extract_tarball(download_path, dest_dir)
    else:
        _unzip(download_path, dest_dir)
    print(f"'{name}' saved at location: {dest_dir} \n")


def get_bs_local_by_platform(temp_dir, scripts_dir):
    platform_url_map = {
        "darwin": BROWSERSTACK_LOCAL_MAC,
        "linux": BROWSERSTACK_LOCAL_LINUX,
        "windows": BROWSERSTACK_LOCAL_WINDOWS,
    }

    bs_filename = "BrowserstackLocal.zip"
    platform_filename_map = {
        "darwin": bs_filename,
        "linux": bs_filename,
        "windows": bs_filename,
    }

    _download_platform_specific_file(
        "Browserstack Local - Latest",
        temp_dir,
        scripts_dir,
        platform_url_map,
        platform_filename_map,
    )


def get_chromedriver_by_platform(temp_dir, scripts_dir):
    print("Getting ChromeDriver latest release version")
    response = requests.get(
        "https://chromedriver.storage.googleapis.com/LATEST_RELEASE"
    )
    latest_release = response.text

    platform_url_map = {
        "darwin": CHROMEDRIVER_MAC % latest_release,
        "linux": CHROMEDRIVER_LINUX % latest_release,
        "windows": CHROMEDRIVER_WINDOWS % latest_release,
    }

    bs_filename = "ChromeDriver.zip"
    platform_filename_map = {
        "darwin": bs_filename,
        "linux": bs_filename,
        "windows": bs_filename,
    }

    _download_platform_specific_file(
        f"Chromedriver - {latest_release}",
        temp_dir,
        scripts_dir,
        platform_url_map,
        platform_filename_map,
    )


def get_geckodriver_by_platform(temp_dir, scripts_dir):
    print("Getting GeckoDriver latest release version")
    response = requests.get(GECKO_DRIVER_LATEST_URL)
    latest_release = json.loads(response.text)["tag_name"]

    platform_url_map = {
        "darwin": GECKODRIVER_MAC % (latest_release, latest_release),
        "linux": GECKODRIVER_LINUX % (latest_release, latest_release),
        "windows": GECKODRIVER_WINDOWS % (latest_release, latest_release),
    }

    platform_filename_map = {
        "darwin": "GeckoDriver.tar.gz",
        "linux": "GeckoDriver.tar.gz",
        "windows": "GeckoDriver.zip",
    }

    _download_platform_specific_file(
        f"Geckodriver - {latest_release}",
        temp_dir,
        scripts_dir,
        platform_url_map,
        platform_filename_map,
    )
