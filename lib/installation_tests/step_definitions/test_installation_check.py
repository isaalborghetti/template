import os

from assertpy import assert_that
from pytest_bdd import scenarios, then

scenarios("../features/cases.feature")


@then("root folder structure is correct")
def check_root_folder():
    assert_that(os.path.isfile("./.editorconfig")).is_true()
    assert_that(os.path.isfile("./.gitignore")).is_true()
    assert_that(os.path.isfile("./.pylintrc")).is_true()
    assert_that(os.path.isfile("./conftest.py")).is_true()
    assert_that(os.path.isfile("./install.sh")).is_true()
    assert_that(os.path.isfile("./install.py")).is_true()
    assert_that(os.path.isfile("./pytest.ini")).is_true()
    assert_that(os.path.isfile("./README.md")).is_true()
    assert_that(os.path.isfile("./requirements.txt")).is_true()


@then("page_objects folder structure is correct")
def check_page_objects_folder():
    assert_that(os.path.isdir("./common/ui/page_objects")).is_true()
    assert_that(os.path.isfile("./common/ui/page_objects/base_component.py")).is_true()
    assert_that(os.path.isfile("./common/ui/page_objects/base_page.py")).is_true()
    assert_that(
        os.path.isfile("./common/ui/page_objects/selenium_generics.py")
    ).is_true()


@then("screenshots folder structure is correct")
def check_screenshots_folder():
    assert_that(os.path.isdir("./output/screenshots")).is_true()
    assert_that(os.path.isdir("./output/screenshots/base")).is_true()


@then("step_definitions folder structure is correct")
def check_step_definitions_folder():
    assert_that(os.path.isdir("./common/ui/step_definitions")).is_true()
    assert_that(
        os.path.isfile("./common/ui/step_definitions/steps_custom.py")
    ).is_true()
    assert_that(os.path.isfile("./common/ui/step_definitions/steps_given.py")).is_true()
    assert_that(os.path.isfile("./common/ui/step_definitions/steps_then.py")).is_true()
    assert_that(os.path.isfile("./common/ui/step_definitions/steps_when.py")).is_true()


@then("test_data folder structure is correct")
def check_test_data_folder():
    assert_that(os.path.isdir("./test_data")).is_true()


@then("utils folder structure is correct")
def check_utils_folder():
    assert_that(os.path.isdir("./utils")).is_true()
    assert_that(os.path.isfile("./utils/env_variables.py")).is_true()
    assert_that(os.path.isfile("./utils/gherkin_utils.py")).is_true()
    assert_that(os.path.isfile("./utils/utils.py")).is_true()
    assert_that(os.path.isfile("./utils/faker_data.py")).is_true()
    assert_that(os.path.isfile("./utils/local_storage.py")).is_true()
    assert_that(os.path.isfile("./utils/browser_actions.py")).is_true()


@then("configs folder structure is correct")
def check_configs_folder():
    assert_that(os.path.isdir("./configs")).is_true()
    assert_that(os.path.isfile("./configs/browser_desktop.json")).is_true()
    assert_that(os.path.isfile("./configs/browser_local.json")).is_true()
    assert_that(os.path.isfile("./configs/browser_mixed.json")).is_true()
    assert_that(os.path.isfile("./configs/browser_mobile.json")).is_true()
    assert_that(os.path.isfile("./configs/.local.env")).is_true()


@then("lib folder structure is correct")
def check_lib_folder():
    assert_that(os.path.isdir("./lib")).is_true()
    assert_that(os.path.isdir("./lib/pytest_testrail_client")).is_true()
    assert_that(os.path.isdir("./lib/installation_scripts")).is_true()

    assert_that(os.path.isfile("./lib/custom_commands.py")).is_true()
    assert_that(os.path.isfile("./lib/pytest_terminal_report.py")).is_true()

    assert_that(
        os.path.isfile("./lib/installation_scripts/download_assets.py")
    ).is_true()


@then("binaries folder structure is correct")
def check_binaries_folder():
    assert_that(os.path.isdir("./binaries")).is_true()
    assert_that(os.path.isdir("./binaries/api")).is_true()
    assert_that(os.path.isdir("./binaries/webdriver")).is_true()
