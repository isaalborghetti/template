import time

import requests
import os
import base64 as b64
import shutil


class SendReport():

   def __init__(self):
       pass

   def fileEncode(self, inputString):
       encodedString = b64.b64encode(inputString)
       return encodedString

   def fileReader(self, filename, projectName):
       with open(projectName+os.sep+filename, 'rb') as file:
           file_bdata = file.read()
           encoded_data = self.fileEncode(file_bdata).decode("utf-8")
       return encoded_data

   def fileHandler(self, projectName):
       payload = {"results": []}
       files = os.listdir(projectName)
       try:
           for file in files:
               filedata = self.fileReader(file, projectName)
               payload['results'].append(
                   {"file_name": file, "content_base64": filedata})
           return payload
       except Exception as e:
           return {'Error': f'Code #{e}. Could not retrive file data.'}

   def generateOnDemandReport(self, projectId, executionFrom, executionName=None, executorType=None):
       headers = {
           'accept': '*/*',
       }

       if executionName != None and executorType != None:
           params = (
               ('project_id', projectId),
               ('execution_from', executionFrom),
               ('execution_name', executionName),
               ('execution_type', executorType),
           )
       else:
           params = (
               ('project_id', projectId),
               ('execution_from', executionFrom)
           )

       response = requests.get(
           'https://qa-reporting-staging.digitalpfizer.com/generate-report', headers=headers, params=params)

       if response.status_code == 200:
           print("On demand report generated successfully.")
       else:
           print("On demand report generation failed.")

   def sendDataToDocker(self, projectAllurePath, projectName):

       file_tuples = self.fileHandler(projectAllurePath)
       headers = {
           'accept': '*/*',
           'Content-Type': 'application/json',
       }

       params = (
           ('project_id', projectName),
           ('force_project_creation', 'true')
       )

       send_url = 'https://qa-reporting-staging.digitalpfizer.com/send-results'

       try:
           sent_response = requests.post(
               send_url,
               headers=headers,
               params=params,
               json=file_tuples
           )
           if sent_response.status_code == 200:
               shutil.rmtree(projectAllurePath)
               print('Folder deleted')
               time.sleep(30)
               self.generateOnDemandReport(projectName, "OSS-Pytest-Plugin")
               print('report generated')
           else:
               print("Failed to upload reports to docker container.")
       except Exception as e:
           print("Docker server is down.")
           print(e)
