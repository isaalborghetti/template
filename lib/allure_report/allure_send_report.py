from restdbclient import SendReport
from pathlib import Path

sendReport = SendReport()
file_path = Path(__file__ + "/../../../output/reports/allure-results").resolve()
sendReport.sendDataToDocker(str(file_path), 'boilerplate')
