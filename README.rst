# Platform Dashboard Test Suite

This README file uses the guidelines recommended in [GitHub Guides to Documenting your Projects on GitHub](https://guides.github.com/features/wikis/).

## Description

The Platform Dashboard Test Suite is based on Python Test Automation Boilerplate and it contains automated functional tests of project Platform Dashboard - Unified Dashboard.
More detailed information about the project can be found on [Confluence Unified Dashboard Project Overview](https://confluence.pfizer.com/display/UD/Project+Overview).


## Extra Useful Links

- [Cloning the Test Boilerplate Repository](https://pfizer.sharepoint.com/sites/DSEQualityTest/SitePages/Cloning-the-test-boilerplate-repository.aspx).

- [Working with this boilerplate](https://pfizer.sharepoint.com/sites/DSEQualityTest/SitePages/Working-with-the-Python-Test-Automation-Boilerplate.aspx) within the [DSE Quality & Test](https://pfizer.sharepoint.com/sites/DSEQualityTest) Sharepoint site.

- Post a message in the [General channel](https://teams.microsoft.com/l/channel/19%3a4161e0292bab4616a0eaca32ab88e45e%40thread.tacv2/General?groupId=fa8a1a93-02b8-449f-858c-342d97610994&tenantId=7a916015-20ae-4ad1-9170-eefd915e9272) of the DSE - Test Guild MS Teams group.



