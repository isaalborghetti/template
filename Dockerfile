FROM python:3

WORKDIR /usr/src/app

RUN /usr/local/bin/python -m pip install --upgrade pip
RUN apt-get -y update

COPY . .

# install google chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
RUN apt-get -y update
RUN apt-get install -y google-chrome-stable

# install chromedriver
RUN apt-get install -yqq unzip
RUN wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip
RUN unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/

# install firefox-esr
RUN apt install firefox-esr -y

# install geckodriver linux
RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-linux64.tar.gz
RUN tar -xvf geckodriver-v0.30.0-linux64.tar.gz
RUN mv geckodriver /usr/local/bin
RUN chmod +x /usr/local/bin/geckodriver


# set display port to avoid crash
ENV DISPLAY=:99

# Install requirements
RUN python -m pip install --upgrade pip
#RUN . env/bin/activate
RUN apt-get install ffmpeg libsm6 libxext6  -y
RUN pip install -r requirements.txt

#CMD python -m pytest --driver Chrome --driver-path /usr/local/bin/chromedriver --capability headless True --tags sample-api-ui-demo --disable-pytest-warnings
#CMD python -m pytest --driver Firefox --driver-path /usr/local/bin/geckodriver --capability headless True --tags sample-api-ui-demo --disable-pytest-warnings
#ENTRYPOINT ["entrypoint.sh"]
