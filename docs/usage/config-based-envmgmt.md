Usage Documentation for Config based environment variables setup
================================================================

Boilerplate framework uses `.local.env` file under `configs/` directory to manage environment variables, especially sensitive data that should not be committed to github repository, and hence would be a part of `.gitignore`. However, there are certain other variables like application url, that would be treated as environment variables due to simplicity of handling them in terms of configuration data. This would also means that these variables should be a part of github 
repository.

Above problem coulbe solved by creating additional `.env` file, and load environment variables from it similar to `.local.env` file. However, in order to be compliant with framework's protocol to have all `.env` files to be not committed to github repo, solution to use `.env` file for this purpose is out of the game.
Second option would have been to choose json file for storing this variables. However, considering the `less`user-friendliness of json files in terms of editing and sticking to the syntax, we propose to use .ini or .toml formats, with +1 on .toml files due to its user friendliness. Read: https://github.com/toml-lang/toml

A envconf.toml file under configs/ folder would be used to hold this configuration, and convert these variables to environment  variables while invoking pytest. Also, its important to have this environment variables created as per the configuration provided. For example, application might have a different base url for QA and Staging configurations.

**TOML File Location: `configs/envconf.toml`**

TOML File Format
----------------

    [config-agnostic]
    TEST-URL="https://global.url"

    [custom-config-1]
    APP-URL="https://custom-one.url"
    HOST="CustomOneHost"

    [custom-config-2]
    APP-URL="https://custom-two.url"
    HOST="CustomTwoHost"

    [custom-config-3]
    APP-URL="https://custom-three.url"
    HOST="CustomThreeHost"

How-To:
-------
**Step 1:**
    User should add necessary constants grouped as per the configuration in the configs/envconfig.toml file, as per above structure defined.
    * Special Key: "custom-agnostic". Group all the constants within custom-agnostic, which has to be added to environment variable irrespective of configurations.
**Step 2:**
    While running pytest, user has to provide a configuration whose constants has to be added to env variables.
    ```
    pytest --env-config=staging
    ```
