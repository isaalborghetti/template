Notable Changes in 3.2
======================

### API Testing

This is a new feature implementation to support API testing within boilerplate framework. Detailed documentation on usage can be found here: [apitesting.md](../usage/apitesting.md)

### Visual Testing

This is a new feature implementation to support Visual Testing using Python Pillow (PIL) library in boilerplate framework. Detailed documentation on usage can be found here: [visualtesting.md](../usage/visualtesting.md)

### Config based environment variables management

This is a new feature implementation to support setting "non-sensitive" environment variables based on certain test configurations like qa, staging, etc. Detailed documentation on usage can be found here: [config-based-envmgmt.md](../usage/config-based-envmgmt.md)

### `common` Folder Structure Change

With release/3.1, common/ directory had two sub-folders for consumer applications consumability, and all were related to ui testing:
* page_objects/
* step_definitions/

In order to support API & Visual testing, and in effort to make the codebase more structured, we made a change to port above two folders and put them in dedicated ui/ subfolders, and have api & visual testing related contents in api/ & visualtesting/ folders respectively.

With the result of this change, users using page_objects/ and its modules have to make modification to their import statements. It has to be noted that, consumer applications need not import common step definitions explicitly in their packages, since it is already handled in root conftest.py file.

Current Import Statements:
```python
from common.page_objects.base_page import BasePage
from common.page_objects.base_component import BaseComponent
from common.page_objects.selenium_generics import SeleniumGenerics
```
Future Import Statements:
```python
from common.ui.page_objects.base_page import BasePage
from common.ui.page_objects.base_component import BaseComponent
from common.ui.page_objects.selenium_generics import SeleniumGenerics
```

### Locator File Format

Up until release/3.1, valid locator json file would have the format: `*locators.json`. It was too restricting, and we decided to make the file naming convention more flexible by adding couple of more valid names to the list. With release/3.2, following names are allowed:
* `*locators.json` (For ex:- `home_page_locators.json`)
* `*locator.json` (For ex:- `home_page_locator.json`)
* `locator*.json` (For ex:- `locators_home.json` or `locator_home.json`)