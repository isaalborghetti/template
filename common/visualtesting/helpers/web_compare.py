from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver

from common.visualtesting import utils as vis_utils
from utils.locators import Locators


def are_two_webelements_look_same(
    base_image_name_with_ext: str,
    selenium: WebDriver,
    locators: Locators,
    locator_path: str,
) -> bool:
    """Function to check if two webelements are same

    Args:
        base_image_name_with_ext: str - Base Image File Name for comparison
        selenium: WebDriver - Selenium driver instance
        locators: Locators
        locator_path: str - locator path of element whose screenshot has to be taken
            to compare with base image.

    Returns:
        Boolean Value indicating if elements are same (True) or different (False)

    """
    file_pths = vis_utils.file_paths(base_image_name_with_ext)
    assert (
        file_pths.base.is_file()
    ), f"Base Image Not present at location {file_pths.base}"

    elem = selenium.find_element(By.XPATH, locators.parse_and_get(locator_path))
    assert (
        elem.is_displayed()
    ), "WebElement @ {locator_path} is not displayed to capture screenshot !!"

    png_screenshot = elem.screenshot_as_png

    with open(file_pths.test, "wb") as f:
        f.write(png_screenshot)
    return vis_utils.are_images_same(base_image_name_with_ext)


def are_two_webpages_look_same(
    base_image_name_with_ext: str,
    selenium: WebDriver,
):
    """Function to check if two web pages look same.

    Args:
        base_image_name_with_ext: str - Base Image Name against which comparison should run.
        selenium: WebDriver - Selenium Driver Instance

    Returns:
        Boolean indicating if the web pages are same.
    """
    file_pths = vis_utils.file_paths(base_image_name_with_ext)
    assert (
        file_pths.base.is_file()
    ), f"Base Image Not present at location {file_pths.base}"

    selenium.save_screenshot(str(file_pths.test))
    return vis_utils.are_images_same(base_image_name_with_ext)
