import re
from typing import Union

import structlog
from pytest import FixtureRequest
from pytest_bdd import parsers, then

from common.api.helpers.assertions import assert_response
from common.api.utils import generic as api_generic_utils
from utils import gherkin_utils, tomlparser

logger = structlog.get_logger(__name__)


@then(
    parsers.parse(
        "I Verify Status Code of request '{name_of_request}' equals '{status_code:d}'"
    )
)
def verify_status_code(
    request: FixtureRequest,
    api_response_container: dict,
    name_of_request: str,
    status_code: int,
):
    """verify_status_code

    Feature File Step Format
    ------------------------
    `Then I Verify Status Code of request 'my-test-request' equals '200'`

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `name_of_request: str` (Required)
        request name for which status code needs to be verified.
    `status_code: int` (Required)
        Similar to username, password should also be stored as environment variable and
        referred here.

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`
    `api_response_container: dict`
        Fixture object defined in `conftest.py` file.

    Step Behavior
    -------------
    * As a prereqisite, assert that the named request is present in api_response_container.
    * If present, check for the status code and verify if it matches expected status code.
    """
    assert (
        request.node.name in api_response_container
    ), "Looks like no requests has been passed during this test!!"
    assert (
        name_of_request in api_response_container[request.node.name]
    ), f"There is no response present with name {name_of_request}"
    res = api_response_container[request.node.name][name_of_request]
    assert_response.assert_status_code(res, status_code)


@then(
    parsers.re("I verify value TYPE:(?P<dataset_reference>.*)", flags=re.S),
    converters=dict(dataset_reference=gherkin_utils.data_table_horizontal_converter),
)
def verify_value_type(
    request: FixtureRequest, api_response_container: dict, dataset_reference: dict
):
    """verify_value_type

    Feature File Step Format
    ------------------------
    `Then I verify value TYPE:`
        `| name_of_request | jsonpath_str  |     type      |`\n
        `| my-get-request  |     $.id      | number (int)  |`

    (excluding back-ticks and \n)

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `dataset_reference: dict` (Required)
        User should be adding datatable in gherkin step in format described above. `pytest-bdd`
        would do the type conversion to dict based on `data_table_horizontal_converter`

        Data Table Columns
        ------------------
        `name_of_request`
            Request Name for verification
        `jsonpath_str`
            Jsonpath of the value that needs to be type checked from response pertaining to request name
        `type`
            Json object type. Note that this is not Python type. Type conversion are as follows:
                `"object" -> dict`
                `"array" -> list`
                `"string" -> str`
                `"number (int)" -> int`
                `"number (real)" -> float`
                `"true" -> True`
                `"false" -> False`
                `"null" -> None`


    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`
    `api_response_container: dict`
        Fixture object defined in `conftest.py` file.

    Step Behavior
    -------------
    * As a prereqisite, assert that the named request is present in api_response_container.
    * If present, fetch value based on provided jsonpath_str from response, and verify type.
    """
    name_of_request = dataset_reference["name_of_request"][0]
    jsonpath_str = dataset_reference["jsonpath_str"][0]
    type_to_assert = dataset_reference["type"][0]

    if type_to_assert not in api_generic_utils.json_python_obj_map:
        logger.error(
            'Invalid Type provided. Defaulting to "string"',
            type_to_assert=type_to_assert,
            json_python_obj_map=api_generic_utils.json_python_obj_map,
        )
        type_to_assert = "string"

    assert (
        request.node.name in api_response_container
    ), "Looks like no requests has been passed during this test!!"
    assert (
        name_of_request in api_response_container[request.node.name]
    ), f"There is no response present with name {name_of_request}"
    res = api_response_container[request.node.name][name_of_request]
    assert_response.assert_value_type(
        res,
        jsonpath_str,
        type_to_assert=api_generic_utils.json_python_obj_map[type_to_assert],
    )


@then(
    parsers.re("I verify value LENGTH:(?P<dataset_reference>.*)", flags=re.S),
    converters=dict(dataset_reference=gherkin_utils.data_table_horizontal_converter),
)
def verify_value_length(
    request: FixtureRequest, api_response_container: dict, dataset_reference: dict
):
    """verify_value_length

    Feature File Step Format
    ------------------------
    `Then I verify value LENGTH:`
        `| name_of_request | jsonpath_str   | length |`\n
        `| my-get-request  |     $.projects |   6    |`

    (excluding back-ticks and \n)

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `dataset_reference: dict` (Required)
        User should be adding datatable in gherkin step in format described above. `pytest-bdd`
        would do the type conversion to dict based on `data_table_horizontal_converter`

        Data Table Columns
        ------------------
        `name_of_request`
            Request Name for verification
        `jsonpath_str`
            Jsonpath of the value that needs to be type checked from response pertaining to request name
        `length`
            expected length of the output sequence

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`
    `api_response_container: dict`
        Fixture object defined in `conftest.py` file.

    Step Behavior
    -------------
    * As a prereqisite, assert that the named request is present in api_response_container.
    * If present, fetch value based on provided jsonpath_str from response, and verify the length
        of the retrieved output.
    """
    name_of_request = dataset_reference["name_of_request"][0]
    jsonpath_str = dataset_reference["jsonpath_str"][0]
    expected_length = dataset_reference["length"][0]
    try:
        expected_length = int(expected_length)
    except ValueError:
        logger.error(
            "Invalid Length Value Provided. Aborting Verification",
            expected_length=expected_length,
        )
        assert False, f"Provided Length Value invalid: {expected_length}"

    assert (
        request.node.name in api_response_container
    ), "Looks like no requests has been passed during this test!!"
    assert (
        name_of_request in api_response_container[request.node.name]
    ), f"There is no response present with name {name_of_request}"
    res = api_response_container[request.node.name][name_of_request]
    assert_response.assert_value_length(
        res, jsonpath_str, expected_length=expected_length
    )


@then(
    parsers.re("I verify value is NON-NULL:(?P<dataset_reference>.*)", flags=re.S),
    converters=dict(dataset_reference=gherkin_utils.data_table_horizontal_converter),
)
def verify_value_non_null(
    request: FixtureRequest, api_response_container: dict, dataset_reference: dict
):
    """verify_value_non_null

    Feature File Step Format
    ------------------------
    `Then I verify value is NON-NULL:`
        `| name_of_request | jsonpath_str |`\n
        `| my-get-request  |  $.projects  |`

    (excluding back-ticks and \n)

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `dataset_reference: dict` (Required)
        User should be adding datatable in gherkin step in format described above. `pytest-bdd`
        would do the type conversion to dict based on `data_table_horizontal_converter`

        Data Table Columns
        ------------------
        `name_of_request`
            Request Name for verification
        `jsonpath_str`
            Jsonpath of the value that needs to be type checked from response pertaining to request name

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`
    `api_response_container: dict`
        Fixture object defined in `conftest.py` file.

    Step Behavior
    -------------
    * As a prereqisite, assert that the named request is present in api_response_container.
    * If present, fetch value based on provided jsonpath_str from response, and verify that
        the value is not None.
    """
    name_of_request = dataset_reference["name_of_request"][0]
    jsonpath_str = dataset_reference["jsonpath_str"][0]
    assert (
        request.node.name in api_response_container
    ), "Looks like no requests has been passed during this test!!"
    assert (
        name_of_request in api_response_container[request.node.name]
    ), f"There is no response present with name {name_of_request}"
    res = api_response_container[request.node.name][name_of_request]
    assert_response.assert_value_non_null(res, jsonpath_str)


@then(
    parsers.re("I verify value NOT EQUALS:(?P<dataset_reference>.*)", flags=re.S),
    converters=dict(dataset_reference=gherkin_utils.data_table_horizontal_converter),
)
def verify_value_not_equals(
    request: FixtureRequest, api_response_container: dict, dataset_reference: dict
):
    """verify_value_not_equals

    Feature File Step Format
    ------------------------
    `Then I verify value NOT EQUALS:`
        `|  name_of_request   |   jsonpath_str      |   not_expected_value  |`\n
        `|  my-request        | $.projects[0].id    |       18              |`\n

    [OR]
    `Then I verify value NOT EQUALS:`
        `|  name_of_request   |   jsonpath_str      |   not_expected_value  |`\n
        `|  my-request        | $.projects[0].id    |   {%PROJECT_ID%}      |`\n

    (excluding back-ticks and \n)

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `dataset_reference: dict` (Required)
        User should be adding datatable in gherkin step in format described above. `pytest-bdd`
        would do the type conversion to dict based on `data_table_horizontal_converter`

        Data Table Columns
        ------------------
        `name_of_request`
            Request Name for verification
        `jsonpath_str`
            Jsonpath of the value that needs to be type checked from response pertaining to request name
        `not_expected_value`
            Test Value that's not to be expected for the output returned through jsonpath

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`
    `api_response_container: dict`
        Fixture object defined in `conftest.py` file.

    Step Behavior
    -------------
    * As a prereqisite, assert that the named request is present in api_response_container.
    * If present, fetch value based on provided jsonpath_str from response, and verify that
        the value is not equal to the `not_expected_value`.
    """
    name_of_request = dataset_reference["name_of_request"][0]
    jsonpath_str = dataset_reference["jsonpath_str"][0]
    not_expected_value = tomlparser.env_formatted(
        dataset_reference["not_expected_value"][0]
    )

    assert (
        request.node.name in api_response_container
    ), "Looks like no requests has been passed during this test!!"
    assert (
        name_of_request in api_response_container[request.node.name]
    ), f"There is no response present with name {name_of_request}"
    res = api_response_container[request.node.name][name_of_request]
    assert_response.assert_value_not_equals(res, jsonpath_str, not_expected_value)


@then(
    parsers.re(
        "I verify values equality for request '(?P<request_name>.*)':(?P<dataset_reference>.*)",
        flags=re.S,
    ),
    converters=dict(dataset_reference=gherkin_utils.data_table_horizontal_converter),
)
def verify_value_equals(
    request: FixtureRequest,
    api_response_container: dict,
    request_name: str,
    dataset_reference: dict,
):
    """verify_value_equals

    Feature File Step Format
    ------------------------
    `Then I verify values equality for request 'my-request':`
        `|   jsonpath_str      |   expected_value  |`\n
        `| $.projects[0].id    |       18          |`\n
        `| $.data              |       my_data     |`

    [OR]
    `Then I verify values equality for request 'my-request':`
        `|   jsonpath_str      |   expected_value  |`\n
        `| $.projects[0].id    |   {%PROJECT_ID%}  |`\n
        `| $.data              |       my_data     |`

    (excluding back-ticks and \n)

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `dataset_reference: dict` (Required)
        User should be adding datatable in gherkin step in format described above. `pytest-bdd`
        would do the type conversion to dict based on `data_table_horizontal_converter`

        Data Table Columns
        ------------------
        `name_of_request`
            Request Name for verification
        `jsonpath_str`
            Jsonpath of the value that needs to be type checked from response pertaining to request name
        `expected_value`
            Test Value that's to be expected for the output returned through jsonpath

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`
    `api_response_container: dict`
        Fixture object defined in `conftest.py` file.

    Step Behavior
    -------------
    * As a prereqisite, assert that the named request is present in api_response_container.
    * If present, fetch value based on provided jsonpath_str from response, and verify that
        the value is equal to the `not_expected_value`.
    """
    test_items = dict(
        zip(
            dataset_reference["jsonpath_str"],
            dataset_reference["expected_value"],
        )
    )
    test_items = {k: tomlparser.env_formatted(v) for k, v in test_items.items()}
    test_results = []

    assert (
        request.node.name in api_response_container
    ), "Looks like no requests has been passed during this test!!"
    assert (
        request_name in api_response_container[request.node.name]
    ), f"There is no response present with name {request_name}"
    res = api_response_container[request.node.name][request_name]
    for jsonpath_str, expected_value in test_items.items():
        try:
            assert_response.assert_value_equals(res, jsonpath_str, expected_value)
        except AssertionError as ae:
            logger.error(
                "Assertion Failed.",
                test_name=request.node.name,
                jsonpath_str=jsonpath_str,
                expected_value=expected_value,
                error_message=ae.args,
            )
            test_results.append(jsonpath_str)
    assert (
        not test_results
    ), f"Some Values in the response not matching expected value: {test_results}"


@then(
    parsers.parse(
        "I verify real number '{test_value:g}' is a member of '{request_set}'"
    )
)
@then(parsers.parse("I verify integer '{test_value:d}' is a member of '{request_set}'"))
@then(parsers.parse("I verify text '{test_value}' is a member of '{request_set}'"))
def verify_value_in(
    request: FixtureRequest,
    api_response_container: dict,
    test_value: Union[str, int, float],
    request_set: str,
):
    """verify_value_in

    Feature File Step Format
    ------------------------
    `Then I verify integer '17' is a member of 'request-name > $.projects[*].id'` # test value is an integer
    `The I verify real number '17.002' is a member of 'request-name > $.projects[*].id'`  # test value is a floating point
    `Then I verify text 'my-value' is a member of 'request-name > $.projects[*].name'`  # test value is a string

    (excluding back-ticks & comments starting with #)

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `test_value`
        Value that needs to be verified. This could be a string, integer or floating point
            depending on the step format specified.
    `request_set`
        This value should be a " > " seperated (note the enclosing spaces) string containing the request name and
            the jsonpath to look out for in the response.

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`
    `api_response_container: dict`
        Fixture object defined in `conftest.py` file.

    Step Behavior
    -------------
    * Split the request set as request name and the jsonpath
    * As a prereqisite, assert that the named request is present in api_response_container.
    * If present, fetch value based on provided jsonpath_str from response, and verify that
        the value is a member of the response container.
    """
    name_of_request, jsonpath_str = request_set.split(" > ")

    assert (
        request.node.name in api_response_container
    ), "Looks like no requests has been passed during this test!!"
    assert (
        name_of_request in api_response_container[request.node.name]
    ), f"There is no response present with name {name_of_request}"

    res = api_response_container[request.node.name][name_of_request]
    assert_response.assert_value_in(res, jsonpath_str, test_value)


@then(
    parsers.parse(
        "I verify real number '{test_value:g}' is not a member of '{request_set}'"
    )
)
@then(
    parsers.parse(
        "I verify integer '{test_value:d}' is not a member of '{request_set}'"
    )
)
@then(parsers.parse("I verify text '{test_value}' is not a member of '{request_set}'"))
def verify_value_not_in(
    request: FixtureRequest,
    api_response_container: dict,
    test_value: Union[str, int, float],
    request_set: str,
):
    """verify_value_not_in

    Feature File Step Format
    ------------------------
    `Then I verify integer '17' is not a member of 'request-name > $.projects[*].id'` # test value is an integer
    `The I verify real number '17.002' is not a member of 'request-name > $.projects[*].id'`  # test value is a floating point
    `Then I verify text 'my-value' is not a member of 'request-name > $.projects[*].name'`  # test value is a string

    (excluding back-ticks & comments starting with #)

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `test_value`
        Value that needs to be verified. This could be a string, integer or floating point
            depending on the step format specified.
    `request_set`
        This value should be a " > " seperated (note the enclosing spaces) string containing the request name and
            the jsonpath to look out for in the response.

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`
    `api_response_container: dict`
        Fixture object defined in `conftest.py` file.

    Step Behavior
    -------------
    * Split the request set as request name and the jsonpath
    * As a prereqisite, assert that the named request is present in api_response_container.
    * If present, fetch value based on provided jsonpath_str from response, and verify that
        the value is not a member of the response container.
    """
    name_of_request, jsonpath_str = request_set.split(" > ")

    assert (
        request.node.name in api_response_container
    ), "Looks like no requests has been passed during this test!!"
    assert (
        name_of_request in api_response_container[request.node.name]
    ), f"There is no response present with name {name_of_request}"

    res = api_response_container[request.node.name][name_of_request]
    assert_response.assert_value_not_in(res, jsonpath_str, test_value)
