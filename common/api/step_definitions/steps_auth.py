"""steps_auth.py

Module for step definitions relating to API authentication.

Boilerplate supports below authentication flows out-of-the-box.
* Basic Authentication.
* Bearer Token Authentication.
* Digest Authentication
"""
from pytest import FixtureRequest
from pytest_bdd import given, parsers
from requests import auth as requests_auth

from common.api.utils import auth as boilerplate_auth
from utils import tomlparser


@given(
    parsers.parse(
        "BASIC Auth is setup with username '{username}' and password '{password}'"
    )
)
def setup_basic_auth(request: FixtureRequest, username: str, password: str):
    """setup_basic_auth

    Feature File Step Format
    ------------------------
    Assuming, username & password are stored in "TEST-USER" & "TEST-PASS" env variables
    respectively, step format in feature file would be (excluding enclosing back-ticks `):

    `Given BASIC Auth is setup with username '{%TEST-USER%}' and password '{%TEST-PASS%}'`
    [OR]
    `Given BASIC Auth is setup with username 'actual_username' and password 'actual_password'`

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `username_envname` (Required)
        username for basic authentication should be stored as an evironmental variable
        and the environment variable name referring the username should be used here.
    `password_envname` (Required)
        Similar to username, password should also be stored as environment variable and
        referred here.

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`

    Step Behavior
    -------------
    * Get username and password from environment variables
    * Instantiate `HTTPBasicAuth` object with username and password
    * create an attribute `request_auth` of `request.node` object
        and save the object generated at previous step

    """
    username = tomlparser.env_formatted(username)
    password = tomlparser.env_formatted(password)
    request.node.request_auth = requests_auth.HTTPBasicAuth(username, password)


@given(
    parsers.parse(
        "DIGEST Auth is setup with username '{username}' and password '{password}'"
    )
)
def setup_digest_auth(request: FixtureRequest, username: str, password: str):
    """setup_digest_auth

    Feature File Step Format
    ------------------------
    Assuming, username & password are stored in "TEST-USER" & "TEST-PASS" env variables
    respectively, step format in feature file would be (excluding enclosing back-ticks `):

    `Given DIGEST Auth is setup with username '{%TEST-USER%}' and password '{%TEST-PASS%}'`
    [OR]
    `Given DIGEST Auth is setup with username 'actual-username' and password 'actual-password'`

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `username_envname` (Required)
        username for digest authentication should be stored as an evironmental variable
        and the environment variable name referring the username should be used here.
    `password_envname` (Required)
        Similar to username, password should also be stored as environment variable and
        referred here.

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`

    Step Behavior
    -------------
    * Get username and password from environment variables
    * Instantiate `HTTPDigestAuth` object with username and password
    * create an attribute `request_auth` of `request.node` object
        and save the object generated at previous step

    """
    username = tomlparser.env_formatted(username)
    password = tomlparser.env_formatted(password)
    request.node.request_auth = requests_auth.HTTPDigestAuth(username, password)


@given(parsers.parse("BEARER Auth is setup with bearer token '{token}'"))
def setup_bearer_auth(request: FixtureRequest, token: str):
    """setup_bearer_auth

    Feature File Step Format
    ------------------------
    Assuming, bearer token is stored in "TEST-BEARER" env variable, step format in
    feature file would be (excluding enclosing back-ticks `):

    `Given BEARER Auth is setup with bearer token '{%TEST-BEARER%}'`
    [OR]
    `Given BEARER Auth is setup with bearer token 'actual-bearer-token-hardcoded'`

    Note: It should be noted that, boilerplate framework does not support generating
    bearer tokens out of the box. It is the responsibility of respective applications
    to generate the token and store it as an environment variable.

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `token_envname` (Required)
        bearer token should be stored as an evironmental variable and the environment
        variable name referring the same should be used here.

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`

    Step Behavior
    -------------
    * Get username and password from environment variables
    * Instantiate `HTTPBasicAuth` object with username and password
    * create an attribute `request_auth` of `request.node` object
        and save the object generated at previous step

    """
    token = tomlparser.env_formatted(token)
    request.node.request_auth = boilerplate_auth.HTTPBearerAuth(token)
