"""steps_request.py

Step Definitions for API requests.
"""
import os
import re
import typing

import structlog
from pytest import FixtureRequest
from pytest_bdd import given, parsers, when

from common.api.helpers import jsonparse, rest
from common.api.helpers.jsonparse import get_value
from common.api.utils import generic as generic_api_utils
from common.api.utils import parse_data
from common.api.utils import stepdefs as stepdef_utils
from common.api.utils.parse_data import ReadCollection
from utils import gherkin_utils, tomlparser

logger = structlog.get_logger(__name__)


@given(parsers.parse("Base Url is set to '{base_url}'"))
def set_base_url(request: FixtureRequest, base_url: str):
    """set_base_url

    This method/step has to be called at **BACKGROUND** only, and this step would
    assign the base url to the whole test case a.k.a scenario, and not at request level.

    Feature File Step Format
    ------------------------
    `Given Base Url is set to 'https://httpbin.org'`
    [OR]
    `Given Base Url is set to '{%MY_BASE_URL%}'`
    where variable `MY_BASE_URL` is an environment variable pointing to actual base url.

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `base_url: str` (Required)
        Url as a hardcoded-string or environment variable referring to base url, enclosed within `{%`
        and `%}`

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`

    Step Behavior
    -------------
    * Provided base_url parameter would be parsed for environment variable value and the
        actual base url would be assigned to `base_url` attribute of `request.node` object,
        which corresponds to the test case being run.
    """
    request.node.base_url = tomlparser.env_formatted(base_url)
    logger.info(
        "Base Url Set Successfully",
        test_case_name=request.node.name,
        base_url=request.node.base_url,
    )


@given(
    parsers.re("Default Headers are set:(?P<dataset_reference>.*)", flags=re.S),
    converters=dict(dataset_reference=gherkin_utils.data_table_horizontal_converter),
)
def set_default_headers(request: FixtureRequest, dataset_reference: dict):
    """set_default_header

    This method/step has to be called at **BACKGROUND** only, and this step would
    assign the default header values to all the requests for a particular test case
    a.k.a scenario, and not at request level.

    Feature File Step Format
    ------------------------
    `Given Default Headers are set:`
        `|     key             |         value          |`
        `|    accept           |    application/json    |`
        `|    allow-cookies    |    true                |`

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `dataset_reference: dict` (Required)
        Default Header values are passed as data table in the format of key-value pairs.

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`

    Step Behavior
    -------------
    * Header Key-Value pairs would be fetched as python dictionaries, and saved as `default_headers`
    within `request.node` object.
    """
    request.node.default_headers = dict(
        zip(dataset_reference["key"], dataset_reference["value"])
    )
    logger.info(
        "Default Headers Set Successfully",
        test_case_name=request.node.name,
        default_headers=request.node.default_headers,
    )


@given(parsers.parse("Endpoint for request '{request_name}' is set to '{endpoint}'"))
def set_request_endpoint(request: FixtureRequest, request_name: str, endpoint: str):
    """set_request_endpoint

    This step has to be defined at request level, in conjunction to `set_base_url` step.
    This step would urlappend the base url specified at background level and the `endpoint`
    specified here and would assign the url as request_args for the `request_name` specified.
    This should be the first & mandatory step of a request, with an exception: If you are not
    setting base url at background and would like for a combined url + endpoint setup for
    request, in which case you would use step def (set_request_url_endpoint).

    Feature File Step Format
    ------------------------
    `Given Endpoint for request 'my-request' is set to 'get'`
    [OR]
    `Given Endpoint for request 'my-request' is set to '{%GET_ENDPOINT%}'

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `endpoint: str` (Required)
        Endpoint to be appended to base url
    `request_name: str` (Required)
        request identifier within the test case.

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`

    Step Behavior
    -------------
    * Checks if `request_args` attribute is present for `request.node` object, and if it isn't,
        create one and assign an empty dictionary to it.
    * Checks if the `request_name` key is present in `request.node.request_args` dictionary object,
        and if it isn't, then create a dictionary with `request_name` key and pre-fill with
        default_headers (this is to handle the case where no request specific headers are required)
        and auth values.
    * urlappend Base Url and endpoint and assign it to "url" key of request_args of the named request.
    """
    stepdef_utils.set_request_args_attr(request)
    stepdef_utils.create_request_args_for_request(request, request_name)
    request.node.request_args[request_name]["url"] = generic_api_utils.urlappend(
        stepdef_utils.get_base_url(request), tomlparser.env_formatted(endpoint)
    )
    logger.info(
        "Endpoint Set Successfully.",
        request_name=request_name,
        request_args=request.node.request_args[request_name],
        test_case_name=request.node.name,
    )


@given(
    parsers.re(
        "Headers for request '(?P<request_name>.*)' are set:(?P<dataset_reference>.*)",
        flags=re.S,
    ),
    converters=dict(dataset_reference=gherkin_utils.data_table_horizontal_converter),
)
def set_request_headers(
    request: FixtureRequest, request_name: str, dataset_reference: dict
):
    """set_request_header

    This step would merge default headers specified at background (if any) and the ones specified
    here and assign to "headers" key of request's request_args. Note that this step should be
    called only after setting base url and/or endpoint for the request.

    Feature File Step Format
    ------------------------
    `Given Headers for request 'my-request' are set:`
        `|     key             |         value          |`
        `|    accept           |    application/json    |`
        `|    allow-cookies    |    true                |`

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `dataset_reference: dict` (Required)
        Default Header values are passed as data table in the format of key-value pairs.
    `request_name: str` (Required)
        request identifier within the test case.

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`

    Step Behavior
    -------------
    * Check for missing url or endpoint (by checking request_args attribute & request_name key in
        request_args). If not raise an exception and abort.
    * Header Key-Value pairs fetched from datatable would be joined with the ones specified as default
        headers and assigned to "headers" key of request's request arguments. Note that, any duplicate
        headers would be overridden here. For example, if "accept" equals "application/json" in default
        headers and the same key is given value of "application/html" in request, the final value would
        be "application/html"
    """

    stepdef_utils.raise_for_mising_url_endpoint(request, request_name)
    headers = dict(zip(dataset_reference["key"], dataset_reference["value"]))
    # set headers within request_args[request_name]
    request.node.request_args[request_name]["headers"] = (
        stepdef_utils.get_default_headers(request) | headers
    )
    logger.info(
        "Request Header Set Successfully.",
        request_name=request_name,
        request_args=request.node.request_args[request_name],
        test_name=request.node.name,
    )


@given(
    parsers.parse(
        "Endpoint Url for request '{request_name}' is set to '{complete_url}'"
    )
)
def set_request_url_endpoint(
    request: FixtureRequest, request_name: str, complete_url: str
):
    """set_request_url_endpoint

    This step has to be defined at request level and would urlappend the `base url` and the `endpoint`
    specified here and would assign the url as request_args for the `request_name` specified. Note that
    base url specified in this step would only apply for the concerned request and not globally, unlike
    the one mentioned at background. This step is useful when you need to override the base url for some
    reason, or using api endpoint from different url other than test app.

    Feature File Step Format
    ------------------------
    Given Endpoint Url for request 'my-request' is set to 'https://httpbin.org/delay/4'
    [OR]
    Given Endpoint Url for request 'my-request' is set to '{%DELAY_URL_ENDPOINT%}'

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `complete_url: str` (Required)
        url endpoint (base url and endpoint combined)
    `request_name: str` (Required)
        request identifier within the test case.

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`

    Step Behavior
    -------------
    * Checks if `request_args` attribute is present for `request.node` object, and if it isn't,
        create one and assign an empty dictionary to it.
    * Checks if the `request_name` key is present in `request.node.request_args` dictionary object,
        and if it isn't, then create a dictionary with `request_name` key and pre-fill with
        default_headers (this is to handle the case where no request specific headers are required)
        and auth values.
    * set complete url to "url" key of request_args of the named request.
    """
    url = tomlparser.env_formatted(complete_url)
    stepdef_utils.set_request_args_attr(request)
    stepdef_utils.create_request_args_for_request(request, request_name)

    # set url within request_args[request_name]
    request.node.request_args[request_name]["url"] = url
    logger.info(
        "Endpoint Url Set Successfully.",
        request_name=request_name,
        request_args=request.node.request_args[request_name],
        test_case_name=request.node.name,
    )


@given(
    parsers.parse("An Url Parameter '{url_param}' is added to request '{request_name}'")
)
def add_url_parameter(request: FixtureRequest, url_param: str, request_name: str):
    """add_url_parameter

    Note that this step depends on the current state of the url value, and would do a ordered modification of
    the url. This step has to be done only after setting base url and endpoint. As an example, if you have a test
    url like this: https://httpbin.org/delay/4/2. then, base url would be `https://httpbin.org` and endpoint would
    be `delay` and the first url parameter would be 4 and second one would be 2, should be called in order.

    Feature File Step Format
    ------------------------
    `Given An Url Parameter '4' is added to request 'my-request'`
    [OR]
    `Given An Url Parameter '{%MY_URL_PARAM%}' is added to request 'my-request'`

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `url_param: str` (Required)
        value to append to url
    `request_name: str` (Required)
        request identifier within the test case.

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`

    Step Behavior
    -------------
    * Check for missing url or endpoint (by checking request_args attribute & request_name key in
        request_args). If not raise an exception and abort.
    * urlappend the existing url and the url parameter for the specified request.
    """
    stepdef_utils.raise_for_mising_url_endpoint(request, request_name)
    request.node.request_args[request_name]["url"] = generic_api_utils.urlappend(
        request.node.request_args[request_name]["url"],
        tomlparser.env_formatted(url_param),
    )

    logger.info(
        "Url Parameter Added Successfully to the request url.",
        url_param=url_param,
        request_name=request_name,
        request_args=request.node.request_args[request_name],
        test_name=request.node.name,
    )


@given(
    parsers.re(
        "Query Parameters are added to request '(?P<request_name>.*)':(?P<dataset_reference>.*)",
        flags=re.S,
    ),
    converters=dict(dataset_reference=gherkin_utils.data_table_horizontal_converter),
)
def add_query_parameter(
    request: FixtureRequest, request_name: str, dataset_reference: dict
):
    """add_query_parameter

    This step has to be done only after setting base url and endpoint. And this would add the parameters
    in the form of key-value pair as "params" key to the request_args.

    Feature File Step Format
    ------------------------
    `Given Query Parameters are added to request 'my-request':
        `|   key   |  value   |`
        `|    q    |   20     |`
        `|    hi   |   hello  |`

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `dataset_reference: dict` (Required)
        key-value pairs of query params
    `request_name: str` (Required)
        request identifier within the test case.

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`

    Step Behavior
    -------------
    * Check for missing url or endpoint (by checking request_args attribute & request_name key in
        request_args). If not raise an exception and abort.
    * Add query params passed in the step as "params" in request arguments.
    """
    stepdef_utils.raise_for_mising_url_endpoint(request, request_name)
    query_params = dict(zip(dataset_reference["key"], dataset_reference["value"]))

    request.node.request_args[request_name]["params"] = query_params

    logger.info(
        "Query Parameters Added Successfully to the request args.",
        request_name=request_name,
        request_args=request.node.request_args[request_name],
        test_name=request.node.name,
    )


@given(
    parsers.parse(
        "Json Payload is loaded from '{payload}' for request '{request_name}'"
    )
)
@given(
    parsers.re(
        "Json Payload is added to request '(?P<request_name>.*)':(?P<payload>.*)",
        flags=re.S,
    ),
    converters=dict(payload=gherkin_utils.data_table_horizontal_converter),
)
def add_json_payload(
    request: FixtureRequest, request_name: str, payload: typing.Union[dict, str]
):
    """add_json_payload

    This step has to be done only after setting base url and endpoint. And this would add the parameters
    in the form of key-value pair as "json" key to the request_args.

    Feature File Step Format
    ------------------------
    `Given Json Payload added to request 'my-request':
        `|   key   |  value   |   type        |`
        `|    q    |   20     |  number (int) |`
        `|    hi   |   hello  |  string       |`

    [OR]
    `Given Json Payload is loaded from 'test.json' for request 'my-request'

    Valid Types are:
    `"string" -> str`
    `"number (int)" -> int`
    `"number (real)" -> float`
    `"true" -> True`
    `"false" -> False`
    `"null" -> None`

    'test.json' is path of json file relative to `test_data/api/payloads` directory.

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `dataset_reference: dict` (Required)
        key-value pairs of data payload
    `request_name: str` (Required)
        request identifier within the test case.

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`

    Step Behavior
    -------------
    * Check for missing url or endpoint (by checking request_args attribute & request_name key in
        request_args). If not raise an exception and abort.
    * Add payload passed in the step as "data" in request arguments.
    """
    stepdef_utils.raise_for_mising_url_endpoint(request, request_name)
    if isinstance(payload, dict):
        # the first use case of adding payload as datatable
        json_payload = parse_data.parse_payload_from_feature_file(payload)
    else:
        # second use case where user specifies json file
        _ = parse_data.ReadJsonPayload(payload)
        json_payload = _.payload

    request.node.request_args[request_name]["json"] = json_payload

    logger.info(
        "Json Payload Added Successfully to the request args.",
        request_name=request_name,
        request_args=request.node.request_args[request_name],
        test_name=request.node.name,
    )


@given(parsers.parse("'{request_type}' http call is made for request '{request_name}'"))
@when(parsers.parse("'{request_type}' http call is made for request '{request_name}'"))
def make_api_verbose_request(
    api_response_container: dict,
    request: FixtureRequest,
    request_type: str,
    request_name: str,
):
    """make_api_verbose_request

    This step has to be called only after setting all required parameters - base url, endpoint,
    headers, params and data as applicable.

    Feature File Step Format
    ------------------------
    This step definition supports both Given and When Steps.

    `Given 'GET' http call is made for request 'my-request'`
    `When 'POST' http call is made for request 'my-request'`

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `request_type` (Required)
        Type of HTTP request. Supported request types are 'GET', 'POST', 'PUT', 'PATCH'
        and 'DELETE'
    `name_of_request` (Required)
        user friendly name for the request

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`
    `api_response_container: dict`
        Fixture object defined in `conftest.py` file.

    Step Behavior
    -------------
    * Check for url and endpoint & request args for the request name
    * Make request using the request_args value for the named request, and
        Save the Response in api_response_container
    """
    stepdef_utils.raise_for_mising_url_endpoint(request, request_name)
    request_type = request_type.upper()
    request_args = request.node.request_args[request_name]
    if request.node.name not in api_response_container:
        api_response_container[request.node.name] = dict()
    response = rest.request_type_map.get(
        request_type, stepdef_utils.request_type_not_implemented
    )(**request_args)
    api_response_container[request.node.name][request_name] = response
    logger.info(
        "Request Successfully made to the server.",
        request_args=request_args,
        response_status_code=response.status_code,
        response_text=response.text or None,
    )


@given(
    parsers.re(
        "Following environment variables are set from request '(?P<request_name>.*)':(?P<dataset_reference>.*)",
        flags=re.S,
    ),
    converters=dict(dataset_reference=gherkin_utils.data_table_horizontal_converter),
)
@when(
    parsers.re(
        "Following environment variables are set from request '(?P<request_name>.*)':(?P<dataset_reference>.*)",
        flags=re.S,
    ),
    converters=dict(dataset_reference=gherkin_utils.data_table_horizontal_converter),
)
def set_env_variables(
    api_response_container: dict,
    request: FixtureRequest,
    request_name: str,
    dataset_reference: dict,
):
    """set_env_variable

    This step has to be called only after sending the request out. Note that, the environment variables
    will be set only if the status code is < 400. This step has to be used in conjunction with verbose
    request mode.

    Feature File Step Format
    ------------------------
    This step definition supports both Given and When Steps.

    `Given Following environment variable are set from request 'my-request':`
    `|   name        |   jsonpath    |`
    `|   PROJECT_ID  |     $.id      |`
    `|  PROJECT_NAME |    $.name     |`

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `dataset_reference: dict` (Required)
        key-value pairs of environment variable name to set and corresponding jsonpath to parse from
            response.
    `name_of_request` (Required)
        user friendly name for the request

    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`
    `api_response_container: dict`
        Fixture object defined in `conftest.py` file.

    Step Behavior
    -------------
    * Check for url and endpoint & request args for the request name
    * Check for response in api_response_container
    * Set Environment Variables based on provided key-value pairs.

    """
    stepdef_utils.raise_for_mising_url_endpoint(request, request_name)
    stepdef_utils.raise_for_missing_response(
        request, request_name, api_response_container
    )
    set_env = dict(zip(dataset_reference["key"], dataset_reference["value"]))
    response = api_response_container[request.node.name][request_name]

    if response.ok:
        for name, value in set_env.items():
            json_value = jsonparse.get_value(response.json(), value)
            os.environ[name] = str(json_value)
            logger.info(
                "Environment Variable added succfullt.",
                var_name=name,
                value=json_value,
            )


@given(
    parsers.parse(
        "I make a '{request_type}' request '{name_of_request}' with dataset '{data_file}'"
    )
)
@when(
    parsers.parse(
        "I make a '{request_type}' request '{name_of_request}' with dataset '{data_file}'"
    )
)
def make_api_collection_request(
    request: FixtureRequest,
    api_response_container: dict,
    request_type: str,
    name_of_request: str,
    data_file: str,
):
    """make_api_collection_request

    This step should be used with all request parameters added to the collection toml file.

    Feature File Step Format
    ------------------------
    This step definition supports both Given and When Steps.

    `Given I make a 'GET' request 'my-get-request' with dataset 'test/get.toml'`
    `When I make a 'POST' request 'my-post-request' with dataset 'test/post.toml'`

    Gherkin Parameters
    ------------------
    Note: These are parameters passed from feature files.
    `request_type` (Required)
        Type of HTTP request. Supported request types are 'GET', 'POST', 'PUT', 'PATCH'
        and 'DELETE'
    `name_of_request` (Required)
        user friendly name for the request
    `data_file` (Required)
        Relative Path of `.toml` file from `test_data/api/collections` (where all API request related data should be stored).
        For example, if your data file is at location `test_data/api/collections/test/get.toml`, then this parameter would be
        `test/get.toml`

    Sample Data File Format
    -----------------------
    ```
        MY-VALUE = "my-formatted value"
        [request-args]
            url = "https://httpbin.org/post"
            json = "installation_tests/test.json"
            [request-args.headers]
                accept = "application/json"

        [set-env]
            PROJECT_ID = "$.id"
    ```
    * Required Keys: [request-args]. Child keys of this dict should contain the key-value pairs
        that would be passed to the requests like url, params, data, headers, etc.
    * [set-env] - any environment variables that has to be set from the response in order to be
        utilized for subsequent requests or external verification should be mentioned here.
    * Any constants, like `MY-VALUE` should be at the beginning of the file.


    Non-Gherkin Parameters
    ----------------------
    `request: FixtureRequest`
    `api_response_container: dict`
        Fixture object defined in `conftest.py` file.

    Step Behavior
    -------------
    * Get dataset (from .toml file) as Dataset object.
    * Check for authentication setup in `requests.node.request_auth`, and if present add auth key to requests_args
    * Create an entry for this test case -> request in api_response_container
    * Request API using request_type and raise NotImplementedError if the request type is unsupported.
    * Save the Response in api_response_container
    * Check for set_env and add required values to environment variables.

    """
    request_type = request_type.upper()
    dataset = ReadCollection(data_file).parsed_collection
    request_args = dataset.request_args
    set_env = dataset.set_env
    if not hasattr(request.node, "request_auth"):
        request_args["auth"] = None
    else:
        request_args["auth"] = request.node.request_auth
    if request.node.name not in api_response_container:
        api_response_container[request.node.name] = dict()
    response = rest.request_type_map.get(
        request_type, stepdef_utils.request_type_not_implemented
    )(**request_args)
    if response.ok and set_env:
        for name, value in set_env.items():
            json_value = get_value(response.json(), value)
            os.environ[name] = str(json_value)
    api_response_container[request.node.name][name_of_request] = response
    logger.info(
        "Request Successfully made to the server.",
        request_args=request_args,
        response_status_code=response.status_code,
        response_text=response.text or None,
    )
