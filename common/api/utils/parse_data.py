"""parse_data.py

Utility to parse api test data toml file
"""
import json
from collections import namedtuple
from pathlib import Path

import fastnumbers as fst

from utils import tomlparser

Dataset = namedtuple("Dataset", "request_args set_env")


class ReadJsonPayload:
    """ReadJsonPayload

    Class Attributes
    ----------------
    `PAYLOADS_PATH: Path`
        Framework defined absolute folder path to store the payloads

    Instance Attributes
    -------------------
    ``payload_path: Path`
        Absolute path of the json file to read payload from.
    `payload: dict`
        Loaded Json data
    """

    PAYLOADS_PATH = Path(__file__ + "../../../../../test_data/api/payloads").resolve()

    def __init__(self, payload_relative_path: str):
        """__init__ for ReadJsonPayload class

        Arguments
        ---------
        `payload_relative_path: str`
            Path to the json file, relative to the PAYLOADS_PATH
        """
        self.payload_path = ReadJsonPayload.PAYLOADS_PATH / payload_relative_path
        self.__raise_for_errors()
        self.payload = self.__read_json_file()

    def __raise_for_errors(self):
        """__raise_for_error

        Raise Exception if json file is not present at the location or the file specified
        doesn't have .json extension
        """
        if (not self.payload_path.is_file()) or (self.payload_path.suffix != ".json"):
            raise Exception(
                f"Unable to find json file @ {self.payload_path}."
                f" Make sure the file is available under {ReadJsonPayload.PAYLOADS_PATH} or its child directories"
                " or the file extension is .json"
            )

    def __read_json_file(self):
        """__read_json_file

        Returns
        -------
        dict
            loaded json data as Python dict
        """
        with open(self.payload_path, "r") as json_payload:
            return json.load(json_payload)


class ReadCollection:
    """ReadCollection

    Class Attributes
    ----------------
    `COLLECTIONS_PATH: Path`
        Framework defined absolute folder path to store the collections

    Instance Attributes
    -------------------
    `collection_path: Path`
        Absolute path of the toml collection file to read request data from.
    `dictified_collection: dict`
        Loaded Toml Data
    `parsed_collection: dict`
        Collection Request Data post parsing for environment variables and constants substitution
    """

    COLLECTIONS_PATH = Path(
        __file__ + "../../../../../test_data/api/collections"
    ).resolve()

    def __init__(self, collection_relative_path: str):
        """__init__ for ReadCollection class

        Arguments
        ---------
        `collection_relative_path: str`
            Path to the toml file, relative to the COLLECTIONS_PATH
        """
        self.collection_path = (
            ReadCollection.COLLECTIONS_PATH / collection_relative_path
        )
        self.dictified_collection = tomlparser.toml2dict(self.collection_path)
        self.parsed_collection = self.__get_dataset_2()

    def __get_dataset_2(self) -> Dataset:
        """get_dataset_2

        Arguments
        ---------
        `test_data_path: str`
            Path to the test data toml file, relative to `API_TEST_DATA_FILES_PATH`

        Returns
        -------
        `Dataset` object containing `request_args` and `set_env`
        """
        assert (
            "request-args" in self.dictified_collection
        ), f"Key request-args not present in data file: {self.collection_path}"

        request_args_ = self.dictified_collection["request-args"]
        payload = request_args_.get("json")
        if payload:
            if isinstance(payload, str):
                # i.e. pointing to json file path
                payload = ReadJsonPayload(payload).payload
        print(payload)
        request_args = {
            "url": request_args_.get("url", ""),
            "headers": request_args_.get("headers", {}),
            "params": request_args_.get("params", None),
            "json": payload or None,
        }
        request_args_.update(request_args)
        set_env = self.dictified_collection.get("set-env", {})
        return Dataset(request_args_, set_env)


def parse_payload_from_feature_file(payload: dict):
    """parse_payload_from_feature_file

    Arguments
    ---------
    `payload: dict`
        key-value pairs of data table provided by used in a feature file

    Returns
    -------
    `dict`
        Type converted dictionary of user defined payload
    """
    assert (req_keys := {"key", "value", "type"}) == (
        actual_keys := set(payload.keys())
    ), f"Required Keys Mismatch: {req_keys.symmetric_difference(actual_keys)}"
    _json_payload = tuple(
        zip(
            payload["key"],
            payload["value"],
            payload["type"],
        )
    )
    _type_cnvrsn_map = {
        "string": str,
        "number (int)": fst.fast_int,
        "number (real)": fst.fast_float,
        "true": lambda _: True,
        "false": lambda _: False,
        "null": lambda _: None,
    }

    return {k: _type_cnvrsn_map.get(t, str)(v) for k, v, t in _json_payload}
