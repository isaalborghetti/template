"""stepdefs.py

API step definitions related utility functions
"""
from pytest import FixtureRequest


class BaseUrlNotSetError(Exception):
    """Exception to be raised when Base URL is not set for a request"""


def request_type_not_implemented(*args, **kwargs):
    raise NotImplementedError


def set_request_args_attr(request: FixtureRequest):
    """set_request_args_attr

    Function to set `request_args` attribute to a test case, a.k.a `request.node` object

    Arguments
    ---------
    `request: FixtureRequest`

    Returns
    -------
    None
    """
    if not hasattr(request.node, "request_args"):
        setattr(request.node, "request_args", {})


def create_request_args_for_request(request: FixtureRequest, request_name: str):
    """create_request_args_for_request

    Function to create request arguments dictionary for a named request for a particular
    test case (`request.node` object represents a test case).

    Arguments
    ---------
    `request: FixtureRequest`
    `request_name: str`

    Returns
    -------
    None
    """
    if request_name not in request.node.request_args:
        request.node.request_args[request_name] = {
            "headers": get_default_headers(request),
            "auth": get_request_auth(request),
        }


def get_base_url(request: FixtureRequest) -> str:
    """get_base_url

    Check if a test case / request.node object has an attribute of base_url. If it isn't,
    raise an exception indicating the same. This is used in steps to set request endpoint,
    in which exception would be raised if endpoint is attempted to set before setting base url

    Arguments
    ---------
    `request: FixtureRequest`

    Returns
    -------
    `request.node.base_url: str`

    Raises
    ------
    `BaseUrlNotSetError`
    """
    if not hasattr(request.node, "base_url"):
        raise BaseUrlNotSetError(
            f"Base Url is not set for test case: {request.node.name}."
            " Set Base Url in your step background using gherkin step definition: "
            "Given Base Url is set to 'your-base-url'"
        )
    return request.node.base_url


def get_default_headers(request: FixtureRequest) -> dict:
    """get_default_headers

    Gets default header dictionary if set, for a test case. If not, return empty dict.

    Arguments
    ---------
    `request: FixtureRequest`

    Returns
    -------
    `request.node.default_headers | dict() : dict`
    """
    if hasattr(request.node, "default_headers"):
        return request.node.default_headers
    return dict()


def raise_for_mising_url_endpoint(request: FixtureRequest, request_name: str):
    """raise_for_mising_url_endpoint

    Url &/ endpoint has to be set first in a test case for a named request, and only then
    request headers, url params, query params and payload has to be set. By design,
    request_args for a request is created during the url/endpoint creation steps. This
    functions checks for the presence of request arguments to indicate if url and/or endpoint
    is set for a particular request.

    Arguments
    ---------
    `request: FixtureRequest`
    `request_name: str`

    Returns
    -------
    None

    Raises
    ------
    `Exception`
        When request_args is not found for a test case object, or request_name is not available
        in request arguments dict.
    """
    if (not hasattr(request.node, "request_args")) or (
        request_name not in request.node.request_args
    ):
        raise Exception(
            "Looks like endpoint is not set. Set URL and Endpoint prior to setting headers!!"
        )


def raise_for_missing_response(
    request: FixtureRequest, request_name: str, api_response_container: dict
):
    """raise_for_mising_response

    After a request is passed, the corresponding response is saved in a dictionary (pytest fixture)
    called api_response_container, which will be used for subsequent verification and other test steps.
    This function checks & raises an exception in case concerned test case or request is not found in
    the container

    Arguments
    ---------
    `request: FixtureRequest`
    `request_name: str`
    `api_response_container: dict`

    Returns
    -------
    None

    Raises
    ------
    `Exception`
    """
    if request.node.name not in api_response_container:
        raise Exception("Looks like no requests has been passed during this test!!")
    if request_name not in api_response_container[request.node.name]:
        raise Exception(f"There is no response present with name {request_name}")


def get_request_auth(request: FixtureRequest):
    """get_request_auth

    Authentication is set common to a particular test case (not at request level). This function
    checks if an authentication is setup and returns the same (or None)

    Arguments
    ---------
    `request: FixtureRequest`

    Returns
    -------
    `request.node.request_auth | None`
    """
    if not hasattr(request.node, "request_auth"):
        return None
    return request.node.request_auth
