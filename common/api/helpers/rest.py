"""API Requests Helper module
"""
from functools import partial
from typing import Union

import requests
import structlog
from requests.auth import AuthBase

logger = structlog.get_logger(__name__)


def api_request(
    *,
    method: str,
    url: str,
    headers: dict,
    params: Union[dict, None] = None,
    json: Union[dict, None] = None,
    auth: Union[AuthBase, None] = None,
    **var_req_args,
) -> requests.Response:
    """Send API Requests

    Positional Arguments
    --------------------
    None

    Keyword-only Arguments
    ----------------------
    `method: str` (Required)
        HTTP Request Method - GET, POST, PUT, PATCH, DELETE, etc.
    `url: str` (Required)
        Server Url Endpoint to communicate with. E.g.:- https://httpbin.org/get
    `headers: dict` (Required)
        key, value pairs of header values to be passed along with request like "accept": "application/json", etc.
    `params: dict | None` (Optional. Defaulted to None)
        Query Parameters as key, value pairs.
    `json: dict | None` (Optional. Defaulted to None)
        Payload that has to be sent to the server as dict.
    `auth: AuthBase | None` (Optional. Defaulted to None)
        Authentication Model for API. Like, Basic Authentication (HTTPBasicAuth), etc.
    `**var_req_args`
        Variable Request Args, corresponding to applicable arguments accepted `requests.request` method.
        For ex: `timeout`, etc.

    Returns
    -------
    `requests.Response` Object.

    Raises
    ------
    `requests.exceptions.RequestException`
        This exception is raised in case of any exceptions raised by requests module, except HTTPError.
    `Exception`
        Generic Exception (Catch-all) that is non-requests.


    """
    return_res = None
    params = params or None
    json = json or None
    try:
        logger.info(
            "Preparing to send API request",
            method=method,
            url=url,
            params=params,
            json=json,
            headers=headers,
            auth=auth,
            additional_args=var_req_args,
        )
        response = requests.request(
            method=method,
            url=url,
            params=params,
            json=json,
            headers=headers,
            auth=auth,
            **var_req_args,
        )
        response.raise_for_status()
        return_res = response
    except requests.exceptions.HTTPError as http_err:
        logger.warning(
            "Received HTTP Error (Status Code >= 400)",
            url=url,
            params=params,
            headers=headers,
            status_code=http_err.response.status_code,
            response_output=http_err.response.text,
        )
        return_res = http_err.response
    except requests.exceptions.RequestException as req_exc:
        logger.error(
            "Server Side Error Observed (non-http error)",
            url=url,
            params=params,
            headers=headers,
            args=req_exc.args,
        )
        raise
    except Exception:
        logger.error("Non-Requests Exception Occurred.")
        raise
    return return_res


# Note: to understand `partial` usage, please read:
# https://docs.python.org/3/library/functools.html#functools.partial
request_type_map = {
    "GET": partial(api_request, method="GET"),
    "POST": partial(api_request, method="POST"),
    "PUT": partial(api_request, method="PUT"),
    "PATCH": partial(api_request, method="PATCH"),
    "DELETE": partial(api_request, method="DELETE"),
}
