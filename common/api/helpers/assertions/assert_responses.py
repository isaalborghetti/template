"""assert_responses.py

This module is used to add custom assertion involving two responses.
"""
from requests import Response

from common.api.helpers import jsonparse
from common.api.helpers.assertions.assert_response import (
    assert_no_client_or_server_error,
)


def assert_value_equals(
    response_one: Response,
    jsonpath_str_one: str,
    response_two: Response,
    jsonpath_str_two: str,
):
    """assert_value_equals

    To verify that an object returned using jsonpath from first response matches a value
    from second response.

    Positional Arguments
    --------------------
    `response_one: Response` (Required)
        First Response object returned as a result of `requests.request` call
    `jsonpath_str_one: str` (Required)
        Jsonpath corresponding to the value that needs to be fetched from json_obj of first response.
    `response_two: Response` (Required)
        Second Response object returned as a result of `requests.request` call
    `jsonpath_str_two: str` (Required)
        Jsonpath corresponding to the value that needs to be fetched from json_obj of second response.

    Returns
    -------
    `None`

    Raises
    ------
    `AssertionError`
        * When status code is greater than or equal to 400 in either of responses.
        * When the values do not match (i.e. value_from_response_one != value_from_response_two)
    """
    assert_no_client_or_server_error(response_one)
    assert_no_client_or_server_error(response_two)

    res_one_json_out = response_one.json()
    res_two_json_out = response_two.json()
    value_from_response_one = jsonparse.get_value(res_one_json_out, jsonpath_str_one)
    value_from_response_two = jsonparse.get_value(res_two_json_out, jsonpath_str_two)

    assert value_from_response_one == value_from_response_two, (
        "Failed Equality Verification: "
        f"response_one_value=({jsonpath_str_one}, {value_from_response_one})"
        " vs "
        f"response_two_value=({jsonpath_str_two}, {value_from_response_two})"
    )


def assert_value_membership(
    response_one: Response,
    jsonpath_str_one: str,
    response_two: Response,
    jsonpath_str_two: str,
):
    """assert_value_membership

    To verify that an object returned using jsonpath from first response
    is a member of an object returned from second response.

    Positional Arguments
    --------------------
    `response_one: Response` (Required)
        First Response object returned as a result of `requests.request` call
    `jsonpath_str_one: str` (Required)
        Jsonpath corresponding to the value that needs to be fetched from json_obj of first response.
    `response_two: Response` (Required)
        Second Response object returned as a result of `requests.request` call
    `jsonpath_str_two: str` (Required)
        Jsonpath corresponding to the value that needs to be fetched from json_obj of second response.

    Returns
    -------
    `None`

    Raises
    ------
    `AssertionError`
        * When status code is greater than or equal to 400 in either of responses.
        * On failed membership test.
    """
    assert_no_client_or_server_error(response_one)
    assert_no_client_or_server_error(response_two)

    res_one_json_out = response_one.json()
    res_two_json_out = response_two.json()
    value_from_response_one = jsonparse.get_value(res_one_json_out, jsonpath_str_one)
    value_from_response_two = jsonparse.get_value(res_two_json_out, jsonpath_str_two)

    assert value_from_response_one in value_from_response_two, (
        "Failed Membership Verification: "
        f"response_one_value=({jsonpath_str_one}, {value_from_response_one})"
        " vs "
        f"response_two_value=({jsonpath_str_two}, {value_from_response_two})"
    )


def assert_value_non_membership(
    response_one: Response,
    jsonpath_str_one: str,
    response_two: Response,
    jsonpath_str_two: str,
):
    """assert_value_non_membership

    To verify that an object returned using jsonpath from first response
    is not a member of an object returned from second response.

    Positional Arguments
    --------------------
    `response_one: Response` (Required)
        First Response object returned as a result of `requests.request` call
    `jsonpath_str_one: str` (Required)
        Jsonpath corresponding to the value that needs to be fetched from json_obj of first response.
    `response_two: Response` (Required)
        Second Response object returned as a result of `requests.request` call
    `jsonpath_str_two: str` (Required)
        Jsonpath corresponding to the value that needs to be fetched from json_obj of second response.

    Returns
    -------
    `None`

    Raises
    ------
    `AssertionError`
        * When status code is greater than or equal to 400 in either of responses.
        * On failed non-membership test.
    """
    assert_no_client_or_server_error(response_one)
    assert_no_client_or_server_error(response_two)

    res_one_json_out = response_one.json()
    res_two_json_out = response_two.json()
    value_from_response_one = jsonparse.get_value(res_one_json_out, jsonpath_str_one)
    value_from_response_two = jsonparse.get_value(res_two_json_out, jsonpath_str_two)

    assert value_from_response_one not in value_from_response_two, (
        "Failed Non-Membership Verification: "
        f"response_one_value=({jsonpath_str_one}, {value_from_response_one})"
        " vs "
        f"response_two_value=({jsonpath_str_two}, {value_from_response_two})"
    )
