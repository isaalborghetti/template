"""assert_response.py

Module to hold some custom assertions for API response.
"""
from collections.abc import MutableSequence

import structlog
from requests import Response

from common.api.helpers import jsonparse

logger = structlog.get_logger(__name__)


def assert_status_code(response: Response, status_code_to_assert: int):
    """assert_status_code

    Positional Arguments
    --------------------
    `response: Response`
        Response object returned as a result of `requests.request` call
    `status_code_to_assert: int`
        Response Status Code that needs to be asserted. Like, 200, 404, etc.

    Returns
    -------
    `None`

    Raises
    ------
    `AssertionError`
        For status code mismatch.
    """
    assert (
        response.status_code == status_code_to_assert
    ), f"Status Code Mismatch. Expected {status_code_to_assert} vs Actual {response.status_code}"


def assert_no_client_or_server_error(response: Response):
    """assert_no_client_or_server_error (keeping this for internal purpose.)

    To assert if response status code is < 400 (response.ok).

    Positional Arguments
    --------------------
    `response: Response`
        Response object returned as a result of `requests.request` call

    Returns
    -------
    `None`

    Raises
    ------
    `AssertionError`
        When status code is greater than or equal to 400.
    """
    assert (
        response.ok
    ), f"There is a server/client error detected. Response Status Code: {response.status_code}"


def assert_value_equals(response: Response, jsonpath_str: str, test_value):
    """assert_value_equals

    To assert if a value from response json (fetched from jsonpath) matches test_value.
    Note that this function does a type conversion to string and then compare equality.
    For example, if the verification is to be done for an integer 5. Then, the response
    value would be type converted to string '5' and compared against stringified
    test_value. If you are interested in asserting type of the output, then do
    `assert_value_type`.

    Positional Arguments
    --------------------
    `response: Response` (Required)
        Response object returned as a result of `requests.request` call
    `jsonpath_str: str` (Required)
        Jsonpath corresponding to the value that needs to be fetched from json_obj.
    `test_value` (Required)
        expected value that has to be asserted for equality.

    Returns
    -------
    `None`

    Raises
    ------
    `AssertionError`
        * When status code is greater than or equal to 400.
        * When the values do not match (i.e. value_from_response != test_value)
    """
    assert_no_client_or_server_error(response)
    json_obj = response.json()
    value_from_response = str(jsonparse.get_value(json_obj, jsonpath_str))
    test_value = str(test_value)
    assert (
        value_from_response == test_value
    ), f"value from response {value_from_response} @ jsonpath {jsonpath_str} != value to assert {test_value}"


def assert_value_not_equals(response: Response, jsonpath_str: str, test_value):
    """assert_value_not_equals

    Note that this function does a type conversion to string and then compare inequality.
    For example, if the verification is to be done for an integer 5 vs "5". Then, the response
    value would be type converted to string '5' and compared against stringified
    test_value. If you are interested in asserting type of the output, then do
    `assert_value_type`.

    Positional Arguments
    --------------------
    `response: Response` (Required)
        Response object returned as a result of `requests.request` call
    `jsonpath_str: str` (Required)
        Jsonpath corresponding to the value that needs to be fetched from json_obj.
    `test_value` (Required)
        expected value that has to be asserted for equality.

    Returns
    -------
    `None`

    Raises
    ------
    `AssertionError`
        * When status code is greater than or equal to 400.
        * When the values match (i.e. value_from_response == test_value)
    """
    assert_no_client_or_server_error(response)
    json_obj = response.json()
    value_from_response = jsonparse.get_value(json_obj, jsonpath_str)
    assert str(value_from_response) != str(
        test_value
    ), f"value from response {value_from_response} @ jsonpath {jsonpath_str} equals value to assert {test_value}"


def assert_value_non_null(response: Response, jsonpath_str: str):
    """assert_value_non_null

    This function is used to assert that the value returned by jsonpath is not null.
    i.e. (value is not None).

    Positional Arguments
    --------------------
    `response: Response` (Required)
        Response object returned as a result of `requests.request` call
    `jsonpath_str: str` (Required)
        Jsonpath corresponding to the value that needs to be fetched from json_obj.

    Returns
    -------
    `None`

    Raises
    ------
    `AssertionError`
        * When status code is greater than or equal to 400.
        * When the value is not None
    """
    assert_no_client_or_server_error(response)
    json_obj = response.json()
    value_from_response = jsonparse.get_value(json_obj, jsonpath_str)
    assert (
        value_from_response is not None
    ), f"value from response {value_from_response} @ jsonpath {jsonpath_str} is null."


def assert_value_type(response: Response, jsonpath_str: str, *, type_to_assert: type):
    """assert_value_type

    This function is used to assert the type of returned output value.

    Positional Arguments
    --------------------
    `response: Response` (Required)
        Response object returned as a result of `requests.request` call
    `jsonpath_str: str` (Required)
        Jsonpath corresponding to the value that needs to be fetched from json_obj.

    Keyword-only Arguments
    ----------------------
    `type_to_assert: type` (Required)
        Expected Type of the object returned by jsonpath.

    Returns
    -------
    `None`

    Raises
    ------
    `AssertionError`
        * When status code is greater than or equal to 400.
        * When the value's type do not match.
    """
    assert_no_client_or_server_error(response)
    json_obj = response.json()
    value_from_response = jsonparse.get_value(json_obj, jsonpath_str)
    if type_to_assert in (True, False, None):
        assert (
            value_from_response is type_to_assert
        ), f"Type Mismatch. Expected {type_to_assert} vs Actual {type(value_from_response)}"
    else:
        assert isinstance(
            value_from_response, type_to_assert
        ), f"Type Mismatch. Expected {type_to_assert} vs Actual {type(value_from_response)}"


def assert_value_length(response: Response, jsonpath_str: str, *, expected_length: int):
    """assert_value_length

    This function is used to verify that the length of record matches expected length

    Positional Arguments
    --------------------
    `response: Response` (Required)
        Response object returned as a result of `requests.request` call
    `jsonpath_str: str` (Required)
        Jsonpath corresponding to the value that needs to be fetched from json_obj.

    Keyword-only Arguments
    ----------------------
    `expected_length: int` (Required)
        Expected length of the object to test.

    Returns
    -------
    `None`

    Raises
    ------
    `AssertionError`
        * When status code is greater than or equal to 400.
        * When the response output does not have length property.
        * When the value's length do not match.
    """
    assert_no_client_or_server_error(response)
    json_obj = response.json()
    value_from_response = jsonparse.get_value(json_obj, jsonpath_str)
    try:
        value_length = len(value_from_response)
    except TypeError:
        logger.error(
            "Cannot Compute Length for output.",
            value_from_response=value_from_response,
            type_of_response_value=type(value_from_response),
        )
        assert (
            False
        ), f"Response Value: {value_from_response} of type {type(value_from_response)}, does not have __len__ property."
    assert (
        value_length == expected_length
    ), f"Length Mismatch. Expected {expected_length} vs Actual {value_length}"


def assert_value_in(response: Response, jsonpath_str: str, test_value):
    """assert_value_in

    This function is used to test positive membership.
    for example: "a" in ["a", 2, 3]

    Positional Arguments
    --------------------
    `response: Response` (Required)
        Response object returned as a result of `requests.request` call
    `jsonpath_str: str` (Required)
        Jsonpath corresponding to the container that needs to be fetched from json_obj.
    `test_value` (Required)
        Expected Value to test membership

    Returns
    -------
    `None`

    Raises
    ------
    `AssertionError`
        * When status code is greater than or equal to 400.
        * When the response output is not a sequence.
        * When the test value is not a member of the response output.
    """
    assert_no_client_or_server_error(response)
    json_obj = response.json()
    value_from_response = jsonparse.get_value(json_obj, jsonpath_str)
    assert isinstance(
        value_from_response, MutableSequence
    ), f"value at jsonpath {jsonpath_str} doesn't look like a sequence type. Aborting!!"
    assert test_value in value_from_response, (
        f"test value {test_value} is not a member of sequence value from response"
        f" {value_from_response} @ jsonpath {jsonpath_str}"
    )


def assert_value_not_in(response: Response, jsonpath_str: str, test_value):
    """assert_value_not_in

    This function is used to test negative membership.
    for example: 5 not in ["a", 2, 3]

    Positional Arguments
    --------------------
    `response: Response` (Required)
        Response object returned as a result of `requests.request` call
    `jsonpath_str: str` (Required)
        Jsonpath corresponding to the container that needs to be fetched from json_obj.
    `test_value` (Required)
        Expected Value to test non-membership

    Returns
    -------
    `None`

    Raises
    ------
    `AssertionError`
        * When status code is greater than or equal to 400.
        * When the response output is not a sequence.
        * When the test value is a member of the response output.
    """
    assert_no_client_or_server_error(response)
    json_obj = response.json()
    value_from_response = jsonparse.get_value(json_obj, jsonpath_str)
    assert isinstance(
        value_from_response, MutableSequence
    ), f"value at jsonpath {jsonpath_str} doesn't look like a sequence type. Aborting!!"
    assert test_value not in value_from_response, (
        f"test value {test_value} is a member of sequence value from response"
        f" {value_from_response} @ jsonpath {jsonpath_str}"
    )
