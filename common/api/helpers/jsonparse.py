"""Module to get value from json object using jsonpath-ng module

For jsonpath-ng usage and tutorials:
* https://pypi.org/project/jsonpath-ng/
* https://github.com/h2non/jsonpath-ng
* https://www.journaldev.com/33265/python-jsonpath-examples
"""
from typing import Union

from jsonpath_ng.ext import parse


def _get_values(json_obj: dict, jsonpath_str: str) -> list:
    """_get_values

    Positional Arguments
    --------------------
    `json_obj: dict` (Required)
        A valid json/python dictionary that needs to be parsed
    `jsonpath_str: str` (Required)
        Jsonpath corresponding to the value that needs to be fetched from json_obj.

    Keyword-only Arguments
    ----------------------
    `None`

    Returns
    -------
    List of match values
    """
    matched_obj = parse(jsonpath_str).find(json_obj)
    if not matched_obj:
        return matched_obj
    return [match.value for match in matched_obj]


def get_value(json_obj: dict, jsonpath_str: str) -> Union[list, str, None]:
    """get_value

    Positional Arguments
    --------------------
    `json_obj: dict` (Required)
        A valid json/python dictionary that needs to be parsed
    `jsonpath_str: str` (Required)
        Jsonpath corresponding to the value that needs to be fetched from json_obj.

    Keyword-only Arguments
    ----------------------
    `None`

    Returns
    -------
    * List of Matched Values (in case of multiple matches) (OR)
    * A matched value as string (in case of singular match) (OR)
    * None (in case of no match)
    """
    m = _get_values(json_obj, jsonpath_str)
    if not m:
        return None
    if len(m) == 1:
        return m[0]
    return m
