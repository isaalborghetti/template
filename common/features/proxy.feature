@nondestructive @automated @sample-proxy
Feature: Tests to verify proxy config
    Sample scenarios to test the proxy config

    Background:
        Given The browser resolution is '1367' per '768'


    @sample-proxy-2
    Scenario: Navigate to canvas dev with proxy enabled
        Given I am on the url 'https://championspfizercom-preview.dev.pfizerstatic.io/'
        Then I expect that the title is not 'Sign in ・ Cloudflare Access'

