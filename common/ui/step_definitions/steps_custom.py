import json
import re
from pathlib import Path

import pytest
import requests
import structlog
from _pytest.fixtures import FixtureRequest
from assertpy import assert_that, soft_assertions
from lxml import etree
from pytest_bdd import given, parsers, then, when
from pytest_selenium_enhancer import CustomWait
from requests import HTTPError
from selenium.webdriver.common.by import By

from common.ui.page_objects.selenium_generics import SeleniumGenerics
from common.visualtesting.helpers.stdalone_compare import are_two_images_look_same
from common.visualtesting.helpers.web_compare import (
    are_two_webelements_look_same,
    are_two_webpages_look_same,
)
from utils import tomlparser
from utils.browser_actions import BrowserActions
from utils.gherkin_utils import (
    data_table_horizontal_converter,
    data_table_vertical_converter,
)
from utils.locators import Locators

logger = structlog.get_logger(__name__)


@given(
    "I navigate to external page <url>", converters=dict(url=tomlparser.env_formatted)
)
@given(
    parsers.re("I navigate to external page '(?P<url>.*)'"),
    converters=dict(url=tomlparser.env_formatted),
)
def navigate_to_external_page(browser: BrowserActions, url):
    browser.go_to_url(url)


@when("I click on <locator_path>")
@when(
    parsers.re("I click on '(?P<locator_path>.*)'"), converters=dict(locator_path=str)
)
def click_on_element(
    selenium_generics: SeleniumGenerics, locators: Locators, locator_path
):
    xpath = locators.parse_and_get(locator_path)
    selenium_generics.scroll_into_view(selector=xpath)
    selenium_generics.click(selector=xpath)


@when("I click on shadowElement <element_path>")
@when(
    parsers.re("I click on shadowElement '(?P<element_path>.*)'"),
    converters=dict(element_path=str),
)
def click_on_shadow_element(
    selenium_generics: SeleniumGenerics, locators: Locators, element_path
):
    selenium_generics.shadow_click(locators.parse_and_get(element_path))


@when("I hover over <locator_path>")
@when(
    parsers.re("I hover over '(?P<locator_path>.*)'"), converters=dict(locator_path=str)
)
def hover_over_element(
    selenium_generics: SeleniumGenerics, locators: Locators, locator_path
):
    xpath = locators.parse_and_get(locator_path)
    selenium_generics.scroll_into_view(selector=xpath)
    selenium_generics.hover(selector=xpath)


@when("I hover over shadowElement <element_path>")
@when(
    parsers.re("I hover over shadowElement '(?P<element_path>.*)'"),
    converters=dict(element_path=str),
)
def hover_over_shadow_element(
    selenium_generics: SeleniumGenerics, locators: Locators, element_path
):
    selenium_generics.shadow_hover(locators.parse_and_get(element_path))


@when(
    parsers.re(
        "I set '(?P<component_locator>.*)' component style:(?P<data_table>.*)",
        flags=re.S,
    ),
    converters=dict(
        html_block=str, paragraph=int, data_table=data_table_vertical_converter
    ),
)
def nextgen_simple_card_style(
    selenium_generics: SeleniumGenerics, component_locator, data_table
):
    selenium_generics.set_component_style(component_locator, data_table)


@when("I collect all urls from sitemap")
def urls_from_sitemap(request: FixtureRequest, base_url):
    response = requests.get(f"{base_url}/sitemap.xml")
    if response.status_code != 200:
        raise HTTPError("Status Code was not 200. Exiting!!")

    root = etree.fromstring(response.content)

    urls = []
    for sitemap in root:
        children = sitemap.getchildren()
        urls.append(children[0].text)

    request.node.scenarioDict["urls"] = urls


@when(parsers.re("I fetch all urls from external site '(?P<site_url>.*)' sitemap"))
def urls_from_sitemap(request: FixtureRequest, site_url):
    logger.info("Collecting sitemap URLs.", site_url=site_url)
    response = requests.get(f"{site_url}/sitemap.xml")
    if response.status_code != 200:
        raise requests.HTTPError("Status Code was not 200. Exiting!!")

    root = etree.fromstring(response.content)

    urls = []
    for sitemap in root:
        children = sitemap.getchildren()
        urls.append(children[0].text)

    request.node.scenarioDict["urls"] = urls


@then(parsers.re("Sitemap urls are fetching"))
def sitemap_urls_loading(request: FixtureRequest):
    urls = request.node.scenarioDict["urls"]
    with soft_assertions():
        for url in urls:
            logger.info("Loading URL", url=url)
            response = requests.get(url, allow_redirects=False)
            assert_that(
                response.url,
                description=f"URL {response.url} is not the same with {url}",
            ).is_subset_of(url)
            assert_that(
                response.status_code,
                description=f"URL {url} load status code is {response.status_code}",
            ).is_less_than_or_equal_to(307)


@then(parsers.re("External site '(?P<site_url>.*)' url is fetching"))
@then(parsers.re("External url '(?P<site_url>.*)' url is fetching"))
def check_external_site_loading(site_url):
    response = requests.get(site_url, allow_redirects=False)
    with soft_assertions():
        assert_that(
            response.status_code,
            description=f"URL {site_url} load status code is {response.status_code}",
        ).is_less_than(300)


@when(
    parsers.re(
        "Sitemap pages, take a full page screenshot with actions:(?P<data_table>.*)",
        flags=re.S,
    ),
    converters=dict(data_table=data_table_vertical_converter),
)
def take_full_page_screenshot_from_sitemap_with_actions(
    request: FixtureRequest, selenium, selenium_generics, base_url, data_table
):
    request.node.scenarioDict["screenshots"] = (
        request.node.scenarioDict["screenshots"]
        if "screenshots" in request.node.scenarioDict
        else []
    )
    pdf_file_name = "{site}_{width}_{height}.pdf".format(
        site=data_table["site"],
        width=selenium.get_window_size()["width"],
        height=selenium.get_window_size()["height"],
    )
    request.node.scenarioDict["pdf_file_name"] = (
        Path(__file__ + f"../../../../../output/screenshots/actual/full_page").resolve()
        / pdf_file_name
    )
    urls = request.node.scenarioDict["urls"]
    custom_wait = CustomWait(selenium)

    for url in urls:
        response = requests.get(url, allow_redirects=False)
        if response.status_code >= 300:
            continue
        selenium.get(url)
        custom_wait.static_wait(10)

        page_size = selenium_generics.page_size()
        if page_size["height"] < 100 or page_size["width"] < 100:
            continue

        actual_screenshot_dir = selenium_generics.get_actual_screenshot_dir(
            url, base_url, "01", data_table
        )
        elements_to_hide = selenium_generics.get_elements_to_hide(data_table)
        device_offset = (
            selenium.capabilities["deviceOffset"]
            if "deviceOffset" in selenium.capabilities
            else 0
        )

        if data_table["pre_action_screenshot"] == "true":
            selenium.get_full_page_screenshot_as_png(
                actual_screenshot_dir, elements_to_hide, device_offset
            )
            request.node.scenarioDict["screenshots"].append(actual_screenshot_dir)
            base_screenshot_dir = actual_screenshot_dir

        for action in json.loads(data_table["actions"]):
            selenium_generics.execute_action(action)

        custom_wait.static_wait(2)
        selenium.execute_script("window.scrollTo({ left: 0, top: 0});")
        custom_wait.static_wait(2)

        actual_screenshot_dir = selenium_generics.get_actual_screenshot_dir(
            url, base_url, "02", data_table
        )
        selenium.get_full_page_screenshot_as_png(
            actual_screenshot_dir, elements_to_hide, device_offset
        )

        if data_table["pre_action_screenshot"] == "true":
            from utils.utils import compare_images

            base_score = 0.999
            score = compare_images(
                None, base_screenshot_dir, actual_screenshot_dir, None, base_score
            )
            if score >= base_score:
                import os

                os.remove(actual_screenshot_dir)
            else:
                request.node.scenarioDict["screenshots"].append(actual_screenshot_dir)


@when(
    parsers.re(
        "Take screenshots of elements with actions:(?P<data_table>.*)", flags=re.S
    ),
    converters=dict(data_table=data_table_vertical_converter),
)
def take_element_screenshot_with_actions(
    request: FixtureRequest, selenium, selenium_generics, base_url, data_table
):
    request.node.scenarioDict["screenshots"] = (
        request.node.scenarioDict["screenshots"]
        if "screenshots" in request.node.scenarioDict
        else []
    )

    custom_wait = CustomWait(selenium)

    actions = json.loads(data_table["actions"])
    if "menu_button" in actions:
        selenium_generics.click(selector=actions["menu_button"])
        custom_wait.static_wait(4)

        actual_screenshot_dir = selenium_generics.get_actual_screenshot_dir(
            base_url, base_url, "megamenu_0", data_table
        )
        selenium.find_element(By.XPATH, actions["menu"]).screenshot(
            actual_screenshot_dir
        )
        request.node.scenarioDict["screenshots"].append(actual_screenshot_dir)

    elements = (
        selenium.find_elements(By.XPATH, actions["hover"])
        if "hover" in actions
        else selenium.find_elements(By.XPATH, actions["click"])
        if "click" in actions
        else []
    )

    i = 1
    for element in elements:
        if "hover" in actions:
            selenium_generics.hover(element=element)
            element = element.find_element(By.XPATH, actions["expanded"])
            custom_wait.static_wait(2)
            image_b = element.screenshot_as_base64
            element = selenium.find_element(
                By.XPATH, '//div[contains(@class, "bg-image")]'
            )
            custom_wait.static_wait(2)
            image_a = element.screenshot_as_base64

            import base64
            from io import BytesIO

            from PIL import Image

            image_a = Image.open(BytesIO(base64.b64decode(image_a)))
            image_b = Image.open(BytesIO(base64.b64decode(image_b)))

            stitched_image = Image.new(
                "RGB", (image_a.width, image_a.height + image_b.height)
            )
            stitched_image.paste(im=image_a, box=(0, 0))
            stitched_image.paste(im=image_b, box=(0, image_a.height))

            i = i + 1
            actual_screenshot_dir = selenium_generics.get_actual_screenshot_dir(
                base_url, base_url, f"megamenu_{i}", data_table
            )
            stitched_image.save(actual_screenshot_dir)
            request.node.scenarioDict["screenshots"].append(actual_screenshot_dir)

        if "click" in actions:
            selenium_generics.click(element=element)
            custom_wait.static_wait(2)
            element = element.find_element(By.XPATH, actions["expanded"])
            actual_screenshot_dir = selenium_generics.get_actual_screenshot_dir(
                base_url, base_url, f"megamenu_{i}", data_table
            )
            element.screenshot(actual_screenshot_dir)
            i = i + 1
            request.node.scenarioDict["screenshots"].append(actual_screenshot_dir)
            if "click" in actions:
                selenium_generics.click(
                    element=selenium.find_elements(By.XPATH, actions["click"])[0]
                )


@then("Element <locator_path> state should be displayed")
@then(
    parsers.re("Element '(?P<locator_path>.*)' state should be displayed"),
    converters=dict(locator_path=str, state=str),
)
def element_is_displayed(
    selenium_generics: SeleniumGenerics, locators: Locators, locator_path
):
    xpath = locators.parse_and_get(locator_path)
    selenium_generics.is_element_visible(xpath)


@then("Element <locator_path> state should be hidden")
@then(
    parsers.re("Element '(?P<locator_path>.*)' state should be hidden"),
    converters=dict(locator_path=str, state=str),
)
def element_is_hidden(
    selenium_generics: SeleniumGenerics, locators: Locators, locator_path
):
    xpath = locators.parse_and_get(locator_path)
    selenium_generics.is_element_invisible(xpath)


@then(
    "Element <locator_path> text should be <expected_text>",
    converters=dict(expected_text=tomlparser.env_formatted),
)
@then(
    parsers.re("Element '(?P<locator_path>.*)' text should be '(?P<expected_text>.*)'"),
    converters=dict(locator_path=str, expected_text=tomlparser.env_formatted),
)
def element_text(
    selenium_generics: SeleniumGenerics, locators: Locators, locator_path, expected_text
):
    xpath = locators.parse_and_get(locator_path)
    selenium_generics.validate_element_text(xpath, expected_text)


@then(
    parsers.re(
        "Element '(?P<locator_path>.*)' style should be:(?P<data_table>.*)", flags=re.S
    ),
    converters=dict(locator_path=str, data_table=data_table_vertical_converter),
)
def header_menu_button_style(
    selenium_generics: SeleniumGenerics, locators: Locators, locator_path, data_table
):
    xpath = locators.parse_and_get(locator_path)
    selenium_generics.validate_element_style(xpath, data_table)


@then(
    parsers.re("ShadowElement '(?P<locator_path>.*)' style should be:(?P<data_table>.*)", flags=re.S),
    converters=dict(locator_path=str, data_table=data_table_vertical_converter),
)
def header_menu_button_style(selenium_generics: SeleniumGenerics, locators: Locators, locator_path, data_table):
    selenium_generics.validate_shadow_element_style(locators.parse_and_get(locator_path),data_table)


@then("Page <page_url> is loaded", converters=dict(page_url=tomlparser.env_formatted))
@then(
    parsers.re("Page '(?P<page_url>.*)' is loaded"),
    converters=dict(page_url=tomlparser.env_formatted),
)
def page_loaded(selenium_generics: SeleniumGenerics, page_url):
    selenium_generics.validate_page_loaded(page_url)


@then(
    parsers.re("Page scroll position is '(?P<scroll_position>.*)'"),
    converters=dict(scroll_position=str),
)
def page_scroll_position(selenium_generics: SeleniumGenerics, scroll_position):
    selenium_generics.validate_scroll_position(scroll_position)


@then(
    parsers.re("Page visual regression is correct:(?P<data_table>.*)", flags=re.S),
    converters=dict(data_table=data_table_horizontal_converter),
)
def page_visual_is_valid(selenium, selenium_generics: SeleniumGenerics, data_table):
    custom_wait = CustomWait(selenium)

    custom_wait.static_wait(2)
    selenium.execute_script("window.scrollTo({ left: 0, top: 0});")
    custom_wait.static_wait(4)

    ss_dir = Path(__file__ + f"../../../../../output/screenshots/").resolve()

    base_screenshot_dir = (
        ss_dir
        / "base"
        / "full_page"
        / f"{data_table['page_name'][0]}_{selenium_generics._file_extension()}"
    )
    actual_screenshot_dir = (
        ss_dir
        / "actual"
        / "full_page"
        / f"{data_table['page_name'][0]}_{selenium_generics._file_extension()}"
    )
    diff_screenshot_dir = (
        ss_dir
        / "diff"
        / "full_page"
        / f"{data_table['page_name'][0]}_{selenium_generics._file_extension()}"
    )

    device_offset = (
        selenium.capabilities["deviceOffset"]
        if "deviceOffset" in selenium.capabilities
        else 0
    )
    elements_to_hide = {
        "start": data_table["start"] if data_table["start"][0] != "None" else None,
        "all": data_table["all"] if data_table["all"][0] != "None" else None,
        "end": data_table["end"] if data_table["end"][0] != "None" else None,
    }
    image_cv2 = selenium.get_full_page_screenshot_as_base64(
        elements_to_hide, device_offset
    )

    from utils.utils import compare_images

    base_score = 0.999
    score = compare_images(
        image_cv2,
        base_screenshot_dir,
        actual_screenshot_dir,
        diff_screenshot_dir,
        base_score,
    )
    assert (
        score >= base_score
    ), f"Actual component screenshot is different from Base with {score}. Diff saved here: {diff_screenshot_dir}"


@then("I generate a pdf for the screenshots taken")
def generate_pdf(request: FixtureRequest):
    from PIL import Image

    screenshots = request.node.scenarioDict["screenshots"]
    pdf_file_name = request.node.scenarioDict["pdf_file_name"]

    images = []
    for screenshot in screenshots:
        images.append(Image.open(screenshot).convert("RGB"))

    first_image = images[0]
    del images[0]
    first_image.save(pdf_file_name, save_all=True, append_images=images)


@then("I generate pdfs for the screenshots taken")
def generate_pdfs(request: FixtureRequest):
    from PIL import Image

    screenshots = request.node.scenarioDict["screenshots"]

    for screenshot in screenshots:
        image = Image.open(screenshot).convert("RGB")
        image.save(screenshot.replace(".png", ".pdf"))


@when(
    parsers.re(
        "I set '(?P<component_locator>.*)' component style:(?P<data_table>.*)",
        flags=re.S,
    ),
    converters=dict(
        html_block=str, paragraph=int, data_table=data_table_vertical_converter
    ),
)
def nextgen_simple_card_style(
    selenium_generics: SeleniumGenerics, component_locator, data_table
):
    selenium_generics.set_component_style(component_locator, data_table)


@then(parsers.parse("I verify images <name> have no visual regression"))
def image_visual_is_valid(name):
    """Step Definition to verify if two images are same (Standalone Visual Testing)

    Both Base Image and Test Image are saved in respective directories as defined by boilerplate
    framework, i.e. output/screenshots/base and output/screenshots/actual directories, with the
    same name (argument name passed in feature file).

    Args:
        name: str - Image Name to perform visual regression.

    Asserts:
        If Base and Test Images are same. Else raises AssertionError.

    """
    assert are_two_images_look_same(name)


@then(
    parsers.re(
        "I verify that element '(?P<locator_path>.*)' is not visually regressed:(?P<data_table>.*)",
        flags=re.S,
    ),
    converters=dict(data_table=data_table_horizontal_converter),
)
def element_visual_is_valid(selenium, locators: Locators, locator_path, data_table):
    """Step Definition to verify if a particular webelement is not visually regressed.

    Base Image should be saved in output/screenshots/base directory (name as provided in
    the data table). Test Function would take screenshot of corresponding webelement as
    provided in locator path, and asserts if it is same as base image.

    Args:
        selenium - driver instance
        locators - Locators instance
        locator_path - as provided in feature file step.
        data_table - retrieved from feature file - scenario - step.

    Asserts:
        If the webelement captured during test is same as the base image provided.
    """
    assert are_two_webelements_look_same(
        data_table["base_image"][0], selenium, locators, locator_path
    )


@then(
    parsers.re(
        "I verify the page is not visually regressed:(?P<data_table>.*)", flags=re.S
    ),
    converters=dict(data_table=data_table_horizontal_converter),
)
def page_visual_is_valid(selenium, data_table):
    assert are_two_webpages_look_same(data_table["base_image"][0], selenium)


@when("I hover over <locator_path1> and click element <locator_path2>")
@when(
    parsers.re("I hover over '(?P<locator_path1>.*)' and click element '(?P<locator_path2>.*)'"), converters=dict(locator_path1=str,locator_path2=str)
)
def hover_over_and_click_sub_menu(
    selenium_generics: SeleniumGenerics, locators: Locators, locator_path1, locator_path2):
    main_menu = locators.parse_and_get(locator_path1)
    sub_menu = locators.parse_and_get(locator_path2)
    selenium_generics.scroll_into_view(selector=main_menu)
    selenium_generics.hover_and_click(selector1=main_menu, selector2=sub_menu)


@when(
    parsers.re("I set text '(?P<text>.*)' to shadowField '(?P<element_path>.*)'"),
    converters=dict(text=tomlparser.env_formatted, element_path=str),
)
def set_text_in_shadow_element(
    selenium_generics: SeleniumGenerics, text, locators: Locators, element_path
):
    selenium_generics.shadow_set_text(text,locators.parse_and_get(element_path))


@then(
    parsers.re("The shadowElement '(?P<locator_path>.*)' text is '(?P<text>.*)'"),
    converters=dict(text=tomlparser.env_formatted, locator_path=str),
)
def check_shadow_element_equals_text(
    selenium_generics: SeleniumGenerics, locators: Locators, locator_path, text
):
    actual_text = selenium_generics.shadow_get_text_from_element(locators.parse_and_get(locator_path))
    assert_that(actual_text).is_equal_to(text)
