import os

from pytest_bdd import scenarios

from common.step_definitions.steps_given import *
from common.step_definitions.steps_then import *
from common.step_definitions.steps_when import *
from common.step_definitions.steps_custom import *


scenarios("../features/cases.feature")


@then("root folder structure is correct")
def check_root_folder():
    assert_that(os.path.isfile("./.editorconfig")).is_true()
    assert_that(os.path.isfile("./.gitignore")).is_true()
    assert_that(os.path.isfile("./.pylintrc")).is_true()
    assert_that(os.path.isfile("./conftest.py")).is_true()
    assert_that(os.path.isfile("./install.sh")).is_true()
    assert_that(os.path.isfile("./pytest.ini")).is_true()
    assert_that(os.path.isfile("./README.md")).is_true()
    assert_that(os.path.isfile("./requirements.txt")).is_true()


@then("page_objects folder structure is correct")
def check_page_objects_folder():
    assert_that(os.path.isdir("./common/page_objects")).is_true()
    assert_that(os.path.isfile("./common/page_objects/base_component.py")).is_true()
    assert_that(os.path.isfile("./common/page_objects/base_page.py")).is_true()
    assert_that(os.path.isfile("./common/page_objects/selenium_generics.py")).is_true()


@then("screenshots folder structure is correct")
def check_screenshots_folder():
    assert_that(os.path.isdir("./output/screenshots")).is_true()
    assert_that(os.path.isdir("./output/screenshots/base")).is_true()


@then("step_definitions folder structure is correct")
def check_step_definitions_folder():
    assert_that(os.path.isdir("./common/step_definitions")).is_true()
    assert_that(os.path.isfile("./common/step_definitions/steps_custom.py")).is_true()
    assert_that(os.path.isfile("./common/step_definitions/steps_given.py")).is_true()
    assert_that(os.path.isfile("./common/step_definitions/steps_then.py")).is_true()
    assert_that(os.path.isfile("./common/step_definitions/steps_when.py")).is_true()
    assert_that(
        os.path.isfile("./common/step_definitions/test_installation_check.py")
    ).is_true()


@then("test_data folder structure is correct")
def check_test_data_folder():
    assert_that(os.path.isdir("./test_data")).is_true()


@then("utils folder structure is correct")
def check_utils_folder():
    assert_that(os.path.isdir("./utils")).is_true()
    assert_that(os.path.isfile("./utils/env_variables.py")).is_true()
    assert_that(os.path.isfile("./utils/gherkin_utils.py")).is_true()
    assert_that(os.path.isfile("./utils/utils.py")).is_true()
    assert_that(os.path.isfile("./utils/faker_data.py")).is_true()
    assert_that(os.path.isfile("./utils/local_storage.py")).is_true()
    assert_that(os.path.isfile("./utils/browser_actions.py")).is_true()


@then("configs folder structure is correct")
def check_configs_folder():
    assert_that(os.path.isdir("./configs")).is_true()
    assert_that(os.path.isfile("./configs/browser_desktop.json")).is_true()
    assert_that(os.path.isfile("./configs/browser_local.json")).is_true()
    assert_that(os.path.isfile("./configs/browser_mixed.json")).is_true()
    assert_that(os.path.isfile("./configs/browser_mobile.json")).is_true()
    assert_that(os.path.isfile("./configs/.local.env")).is_true()


@then("lib folder structure is correct")
def check_lib_folder():
    assert_that(os.path.isdir("./lib")).is_true()
    assert_that(os.path.isdir("./lib/pytest_testrail_client")).is_true()
    assert_that(os.path.isdir("./lib/installation_scripts")).is_true()
    assert_that(os.path.isdir("./lib/installation_scripts/platform_specific")).is_true()

    assert_that(os.path.isfile("./lib/custom_commands.py")).is_true()
    assert_that(os.path.isfile("./lib/pytest_terminal_report.py")).is_true()

    assert_that(
        os.path.isfile("./lib/installation_scripts/download_assets.py")
    ).is_true()
    assert_that(
        os.path.isfile("./lib/installation_scripts/install_drivers.py")
    ).is_true()
    assert_that(
        os.path.isfile("./lib/installation_scripts/setup_env_configs.py")
    ).is_true()

    assert_that(
        os.path.isfile(
            "./lib/installation_scripts/platform_specific/platform_validation.sh"
        )
    ).is_true()


@then("binaries folder structure is correct")
def check_binaries_folder():
    assert_that(os.path.isdir("./binaries")).is_true()
    assert_that(os.path.isdir("./binaries/api")).is_true()
    assert_that(os.path.isdir("./binaries/webdriver")).is_true()
