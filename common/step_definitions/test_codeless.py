import os

from pytest_bdd import scenarios

from common.step_definitions.steps_given import *
from common.step_definitions.steps_then import *
from common.step_definitions.steps_when import *
from common.step_definitions.steps_custom import *

PROJECT_DIR = os.getcwd()
scenarios("../features/proxy.feature", os.path.join(PROJECT_DIR, "codeless_project/features"), )
