assertpy==1.1
allure-pytest==2.8.40
configparser==5.0.1
cucumber-tag-expressions==4.0.2
diffimg==0.2.3
python-dotenv==0.15.0
Faker==4.14.2
imutils==0.5.3
py==1.9.0
pylint==2.7.0
pytest==6.2.5
pytest-base-url==1.4.2
pytest-bdd==4.0.2
pytest-rerunfailures==9.1.1
pytest-xdist==2.1.0
pytest-logger==0.5.1
pytest-failed-screenshot==1.0.0
Appium-Python-Client==1.0.2
browserstack-local==1.2.2
gherkin-official==4.1.3
selenium==3.141.0
pytest-selenium==2.0.1
webdrivermanager==0.9.0
SSIM-PIL==1.0.10
