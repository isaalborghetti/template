#!/bin/sh

# Gherkin tags for tests to be run
TAGS=$1
if [[ $TAGS = "" ]]
then
  TAGS="sanity"
fi

# Setting up json file of browser capabilities
CONFIG_JSON_FILE=$2
echo "./configs/$CONFIG_JSON_FILE"
if [[ $CONFIG_JSON_FILE = "" ]]
then
    CONFIG_JSON_FILE="./configs/browser_local.json"
else
    CONFIG_JSON_FILE="./configs/$CONFIG_JSON_FILE"
fi

# Number of threads to use when tests are run in parallel
THREADS=$3
if [[ $THREADS = "" ]]
then
  THREADS=5
fi

# Activate Virtual Environment
echo "Activating Virtual Environment .bp-venv..."
echo "\n"
PLATFORM_TYPE=$(command uname)
RETCODE=$?
if [ $RETCODE = 0 ]; then

    if [ "$PLATFORM_TYPE" = "Darwin" ] || [ "$PLATFORM_TYPE" = "Linux" ]; then
        . $HOME/.bp-venv/bin/activate
    else
        . $HOME\\.bp-venv\\Scripts\\activate
    fi
else
    true
fi

echo "Running tests tagged with $TAGS"
# To generate the allure report on the server
ALLURE_DIR=$4
if [[ $ALLURE_DIR = "" ]]
then
    python -m pytest -v --gherkin-terminal-reporter --tags "$TAGS" --device-matrix "$CONFIG_JSON_FILE" -n "$THREADS"
else
    echo "allure report server"
    python -m pytest -v --gherkin-terminal-reporter --tags "$TAGS" -n "$THREADS" --device-matrix "$CONFIG_JSON_FILE" --alluredir=output/reports/allure-results
#    pip install git+ssh://git@github.com/pfizer/OSS-Pytest-Plugin.git@allure-report-package#egg=allure_oss
    python ./lib/allure_report/allure_send_report.py
fi
