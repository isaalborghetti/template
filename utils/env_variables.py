import io
import os
from pathlib import Path
from typing import Union

import toml
from dotenv import load_dotenv

from utils.singleton import SingletonMeta


def load_sensitive_env_from_local_dotenv_file(
    dotenv_file_path: Union[str, None] = None
):
    """Function to load environment variables from `.local.env` file

    Args:
        dotenv_file_path: str | None (Default) - Absolute Path of the .local.env file.

    Returns:
        None
    """
    if dotenv_file_path is None:
        dotenv_file = Path(__file__ + "/../../configs/.local.env").resolve()
    else:
        dotenv_file = Path(dotenv_file_path).resolve()

    if dotenv_file.is_file():
        load_dotenv(dotenv_path=dotenv_file, verbose=True)


def load_non_sensitive_env_from_config(configuration: Union[str, None] = None):
    """Function to set environment variables for provided configuration

    Args:
        configuration: str - Should be the name of Configuration Class

    Returns:
        None
    """
    envconf_file_path = Path(__file__ + "/../../configs/envconf.toml").resolve()

    if envconf_file_path.is_file():
        with open(envconf_file_path, "r") as f:
            envconf = toml.load(f)

        configurations = [key for key, _ in envconf.items() if key != "config-agnostic"]

        if configuration and (configuration not in configurations):
            print(
                f"Provided Configuration `{configuration}` not part of available configurations"
                f"`{','.join(configurations)}`."
                "Adding only config-agnostic constants to environment variables."
            )

        env_conf_dict = envconf.get(configuration, {}) | envconf.get(
            "config-agnostic", {}
        )

        # for setting environment variable
        stream_ = ""
        for name, value in env_conf_dict.items():
            stream_ += f"{name}={value}\n"

        print(f"Setting Environment Variables: {stream_}")

        load_dotenv(stream=io.StringIO(stream_))


class EnvVariables(metaclass=SingletonMeta):
    def __init__(
        self,
        *,
        local_dotenv_file_path: Union[str, None] = None,
        env_config: Union[str, None] = None,
    ) -> None:
        self.env = local_dotenv_file_path
        self.envconf = env_config
        load_sensitive_env_from_local_dotenv_file(self.env)
        load_non_sensitive_env_from_config(self.envconf)

    @property
    def base_url(self):
        return os.getenv("BASE_URL", "")

    @property
    def basic_auth_username(self):
        return os.getenv("BASIC_AUTH_USERNAME", "")

    @property
    def basic_auth_password(self):
        return os.getenv("BASIC_AUTH_PASSWORD", "")

    @property
    def login_username(self):
        return os.getenv("LOGIN_USERNAME", "")

    @property
    def login_password(self):
        return os.getenv("LOGIN_PASSWORD", "")

    def get(self, var_name, default=None) -> str:
        return os.getenv(var_name, default)
