from unified_dashboard_tests.page_objects.base_page import BasePage
from unified_dashboard_tests.page_objects.locators import LoginPageLocators
from selenium.webdriver.common.by import By
import requests


class LoginPage(BasePage):
    def __init__(self, selenium, base_url, env_variables, page_name='LoginPage'):
        super().__init__(selenium, base_url)
        self._page_name = page_name
        self._page_url = 'login#'
        self._env_variables = env_variables

    @property
    def _user_txt_field(self):
        return self._selenium.find_element(By.ID, LoginPageLocators.txtinput_email_id)

    @property
    def _password_txt_field(self):
        return self._selenium.find_element(By.ID, LoginPageLocators.txtinput_password_id)

    @property
    def _login_button(self):
        return self._selenium.find_element(By.ID, LoginPageLocators.button_login_id)

    def input_email(self):
        self._wait.wait_for_element_visible(By.ID, LoginPageLocators.txtinput_email_id)
        self._user_txt_field.click()
        self._user_txt_field.send_keys(self.json_in_array_user_name())

    def input_password(self):
        self._wait.wait_for_element_visible(By.ID, LoginPageLocators.txtinput_password_id)
        self._password_txt_field.click()
        self._password_txt_field.send_keys(self.json_in_array_password())

    def click_login_button(self):
        self._wait.wait_for_element_visible(By.ID, LoginPageLocators.button_login_id)
        self._login_button.click()

    def json_in_array_user_name(self):
        req = requests.get("https://pastebin.com/raw/LQWRi88N")
        response = req.json()
        for item in response:
            user_name = (item['username'])
        return user_name

    def json_in_array_password(self):
        req = requests.get("https://pastebin.com/raw/LQWRi88N")
        response = req.json()
        for item in response:
            password = (item['password'])
        return password

    def check_error_message(self):
        self._wait.wait_for_element_visible(By.CLASS_NAME, LoginPageLocators.error_message_class)
        is_error_displayed = self._selenium.find_element(By.CLASS_NAME, LoginPageLocators.error_message_classs)\
            .is_displayed()
        assert is_error_displayed is False
