class LoginPageLocators:
    txtinput_email_id: str = "user-name"
    txtinput_password_id = "password"
    button_login_id = "login-button"
    error_message_class: str = "error-button"

class HomePageLocators:
    title_class: str = "title"


class CommonLocators:
    success_notification_xpath = "//div[@class='v-snack__wrapper v-sheet theme--dark green']"
    info_notification_xpath = "//div[@class='v-snack__wrapper v-sheet theme--dark blue']"
    warning_notification_xpath = "//div[@class='v-snack__wrapper v-sheet theme--dark yellow']"
    error_notification_xpath = "//div[@class='v-snack__wrapper v-sheet theme--dark red']"
    logout_submenu_xpath = "//div[text()='Log Out']"
    pin_btn_xpath = "(//button[contains(@class,'unpinned')])[1]"
    unpin_btn_xpath = "//button[@class='v-btn v-btn--icon v-btn--round theme--light v-size--default pinned']"
    card_title_xpath = "(//button[contains(@class,'unpinned')]/ancestor::div[contains(@class,'sticky-bottom')]/preceding-sibling::div//div[contains(@class,'card-title')])[1]"
    card_xpath = "//div[contains(@class,'card-overflow')]"
    clickable_card_title_xpath = "//div[contains(@class, 'card-title')]//*"
    loading_status_xpath = "//div[@data-loading='false']"

    def create_dynamic_search_results_projects(project_name):
        dynamic_result_xpath = "//div[text()= '"+ project_name +"']"
        return dynamic_result_xpath

    def create_dynamic_search_results_assets(asset_name):
        dynamic_search_asset_result = "//div[text()= '"+ asset_name +"']"
        return dynamic_search_asset_result

    def create_dynamic_card_type_xpath(card_type):
        dynamic_card_type_xpath = "//div[@card-type='" + card_type +"']"
        return dynamic_card_type_xpath

    def create_dynamic_pin_button_xpath(card_type):
        dynamic_pin_button_xpath = "//div[@card-type='" + card_type + "']//button[@button-type='pin']"
        return dynamic_pin_button_xpath

    def create_dynamic_pin_card_by_title_xpath(card_title):
        dynamic_pin_button_xpath = "//button[contains(@button-type, 'pin')]/ancestor::div//a[contains(text(), '" + card_title + "')]"
        return dynamic_pin_button_xpath

    def create_dynamic_search_pin_button_xpath(asset_name):
        dynamic_search_pin_unpin_button_xpath = "//div[contains(text(),'"+asset_name+"')]/../..//i"
        return dynamic_search_pin_unpin_button_xpath


