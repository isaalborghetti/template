from unified_dashboard_tests.page_objects.base_page import BasePage
from unified_dashboard_tests.page_objects.locators import HomePageLocators
from selenium.webdriver.common.by import By


class HomePage(BasePage):

    def __init__(self, selenium, base_url, env_variables, page_name='HomePage'):
        super().__init__(selenium, base_url)
        self._page_name = page_name
        self._page_url = '#'
        self._env_variables = env_variables

    # Check Home Page
    def check_home_page_loaded(self):
        self._wait.wait_for_element_visible(By.CLASS_NAME, HomePageLocators.title_class)
        is_title_displayed = self._selenium.find_element(By.CLASS_NAME, HomePageLocators.title_class).is_displayed()
        assert is_title_displayed is True
        return True
