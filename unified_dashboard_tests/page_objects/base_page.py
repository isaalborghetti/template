import re
from pytest_selenium_enhancer import CustomWait
from unified_dashboard_tests.page_objects.locators import CommonLocators
from selenium.webdriver.common.by import By


class BasePage:

    def __init__(self, selenium, base_url):
        self._base_url = base_url
        self._selenium = selenium
        self._os = re.sub(r"[\s]*", "", selenium.desired_capabilities['platformName'].lower()) \
            if 'platformName' in selenium.desired_capabilities else \
            re.sub(r"[\s]*", "", selenium.desired_capabilities['platform'].lower())
        if self._os == 'macos':
            self._os = 'macosx'
        self._device = 'mobile' if self._os in ['android', 'ios'] else 'desktop'
        self._wait = CustomWait(self._selenium)

    global card_name
    global is_staging_env
    sso_login_url = "logonsso.pfizer.com/IAMCentralAuthn/IWA/IWAResource1.aspx"

    def open(self, **kwargs):
        pass

    def is_loaded(self, **kwargs):
        pass

    def get_notification_message(self, notification_type):
        parsed_locator = notification_type + "_notification_xpath"
        self._wait.wait_for_element_visible(By.XPATH, getattr(CommonLocators, parsed_locator))
        notification_message = (self._selenium.find_element(By.XPATH, getattr(CommonLocators, parsed_locator))).text
        return notification_message
