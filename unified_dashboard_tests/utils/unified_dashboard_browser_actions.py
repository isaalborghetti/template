import time
from typing import List
import logging
from urllib.parse import quote
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from common.ui.step_definitions.steps_when import *
from unified_dashboard_tests.page_objects.base_page import BasePage
from selenium.webdriver.common.alert import Alert
from unified_dashboard_tests.page_objects.home_page import HomePage
from dotenv import load_dotenv
from unified_dashboard_tests.page_objects.locators import HomePageLocators, CommonLocators, LoginPageLocators, AssetPageLocators, SSOLoginLocators
import os
from utils.browser_actions import BrowserActions
from utils.env_variables import EnvVariables
import base64
from selenium.webdriver.support import expected_conditions as EC

logger = logging.getLogger(__name__)


class UnifiedDashboardBrowserActions:

    def __init__(self, driver):
        self._driver: WebDriver = driver

    def execute_script(self, command):
        self._driver.execute_script(command)

    def scroll_to_element(selenium, locator_path):
        CustomWait(selenium).wait_for_element_clickable(value=CommonLocators.card_xpath, timeout=15)
        element = selenium.find_element(By.XPATH, locator_path)
        script = 'arguments[0].scrollIntoView({block: "center", inline: "center"})'
        selenium.execute_script(script, element)

    def pin_first_card(selenium):
        CustomWait(selenium).wait_for_element_present(value=CommonLocators.card_xpath)
        pin = selenium.find_element_by_xpath(CommonLocators.pin_btn_xpath)
        pin.click()

    def unpin_first_card(selenium):
        CustomWait(selenium).wait_for_element_present(value=CommonLocators.card_xpath)
        unpin = selenium.find_element_by_xpath("//button[@class='v-btn v-btn--icon v-btn--round theme--light v-size--default pinned']")
        unpin.click()

    def save_card_title(selenium):
        BasePage.card_name = selenium.find_element_by_xpath(CommonLocators.card_title_xpath).text

    def save_unpin_card_title(selenium):
        BasePage.card_name = selenium.find_element_by_xpath(CommonLocators.card_title_xpath).text

    def validate_card_pinned_in_dashboardpage(selenium):
        selenium.find_element_by_css_selector(HomePageLocators.top_nav_bar_dashboard_button_css).click()
        CustomWait(selenium).wait_for_element_present(value=f"//button[contains(@class,'pinned')]/ancestor::div["
                                                            f"contains(@class,"
                                                            f"'sticky-bottom')]/preceding-sibling::div//*[contains("
                                                            f"text(),'{BasePage.card_name}')]")

    def validate_card_pinned_on_dashboard_page(selenium, asset_name):
        selenium.find_element_by_css_selector(HomePageLocators.top_nav_bar_dashboard_button_css).click()
        CustomWait(selenium).wait_for_element_present(value=f"//button[contains(@class,'pinned')]/ancestor::div["
                                                            f"contains(@class,"
                                                            f"'sticky-bottom')]/preceding-sibling::div//*[contains("
                                                            f"text(),'" + asset_name + "')]")



    def validate_card_unpinned_in_dashoardpage(selenium):
        selenium.find_element_by_css_selector(HomePageLocators.top_nav_bar_dashboard_button_css).click()
        CustomWait(selenium).wait_for_element_not_visible(value=f"//button[contains(@class,'pinned')]/ancestor::div["
                                                            f"contains(@class,"
                                                            f"'sticky-bottom')]/preceding-sibling::div//*[contains("
                                                            f"text(),'{BasePage.card_name}')]")

    def validate_card_unpinned_on_dashboard_page(selenium, asset_name):
        selenium.find_element_by_css_selector(HomePageLocators.top_nav_bar_dashboard_button_css).click()
        CustomWait(selenium).wait_for_element_not_visible(value=f"//button[contains(@class,'pinned')]/ancestor::div["
                                                            f"contains(@class,"
                                                            f"'sticky-bottom')]/preceding-sibling::div//*[contains("
                                                            f"text(),'" + asset_name + "')]")

    # Include two different logins for staging and production due some differences from the SSO login method from Pfizer
    def login_SSO(selenium):
        load_dotenv()
        username = os.getenv("SSO_USERNAME")
        password_encrypted = os.getenv("SSO_PASSWORD")
        password = base64.b64decode(password_encrypted).decode('ascii')
        if BasePage.is_staging_env == True:
            selenium.find_element_by_id(SSOLoginLocators.input_username_id).send_keys(username)
            selenium.find_element_by_id(SSOLoginLocators.input_password_id).send_keys(password)
            selenium.find_element_by_css_selector(SSOLoginLocators.submit_button_css).click()
        else:
            SSO_url = f"http://{username}:{quote(password)}@{BasePage.sso_login_url}"
            selenium.get(SSO_url)
        CustomWait(selenium).wait_for_element_present(value=HomePageLocators.logo_xpath)

    # Include two different logins for staging and production due some differences from the SSO login method from Pfizer
    def go_SSO_credentials(selenium):
        current_url = selenium.current_url
        selenium.find_element_by_xpath(LoginPageLocators.button_login_with_credentials_xpath).click()
        if "staging" in current_url:
            BasePage.is_staging_env = True
            CustomWait(selenium).wait_for_element_visible(By.ID, value="password")
        else:
            BasePage.is_staging_env = False

