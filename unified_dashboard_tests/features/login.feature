@login @automated @sanity @regression @nondestructive

Feature: Swag Labs Login
    As an user
    I want to be able to login to the Swag Labs
    So I can easily authenticate to the page

    Background:
        Given I am on the page 'https://www.saucedemo.com/'


    Scenario: Login with JSON credentials
        When I login with credential
        Then the login is concluded

