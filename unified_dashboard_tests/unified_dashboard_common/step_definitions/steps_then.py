from pytest_bdd import then, parsers


# Check Login
@then(parsers.parse('the login is concluded'))
def check_login_concluded(login_page, home_page):
    if home_page.check_home_page_loaded() is True:
        return True
    else:
        login_page.check_error_message()

