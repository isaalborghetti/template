from pytest_bdd import when, parsers


# Do Login
@when(parsers.parse('I login with credential'))
def enter_login_data(login_page):
    login_page.input_email()
    login_page.input_password()
    login_page.click_login_button()
