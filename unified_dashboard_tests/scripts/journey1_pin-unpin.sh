#!/bin/sh

echo "Running Journey 1 - Pin/Unpin \n"
echo "===============================================================================\n"
cd ../..
python3 -m pytest unified_dashboard_tests/step_definitions/test_pin.py -v --device-matrix configs/browser_local.json  --base-url https://dashboard-staging.platforms.pfizer  --html=output/reports/report-journey1.html  --gherkin-terminal-reporter --tags "journey1_pin-unpin"
echo "\n"
echo "Execution finished\n"
echo "Report file: /platform-dashboard-test-suite/output/reports/report-journey1.html"
echo "===============================================================================\n"
