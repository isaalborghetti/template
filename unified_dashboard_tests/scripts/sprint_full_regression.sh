#!/bin/sh
echo "===============================================================================\n"
echo "Running Test Automation - Sprint Full Regression Suite\n"
echo "===============================================================================\n"
echo "\n"
echo "Test Automation Setup"
newman run ./unified_dashboard_tests/integration_tests/TestAutomation_Setup.postman_collection.json -e ./unified_dashboard_tests/integration_tests/TestAutomation_Env.postman_environment.json -r htmlextra,cli --reporter-htmlextra-export ./output/reports/setup.html --reporter-htmlextra-darkTheme  --timeout-request 120000 --export-environment ./unified_dashboard_tests/integration_tests/TestAutomation_Env.postman_environment.json

echo "\n"
echo "Full Regression Execution"
python3 -m pytest unified_dashboard_tests/step_definitions/*  -v --device-matrix configs/browser_local.json --base-url https://dashboard-staging.platforms.pfizer --html=report-fullregression.html --gherkin-terminal-reporter --pytest-testrail-export-test-results --pytest-testrail-test-plan-id 9848 --pytest-testrail-test-configuration-name "macOS BigSur - Chrome"

echo "\n"
echo "Test Automation Teardown"
newman run ./unified_dashboard_tests/integration_tests/TestAutomation_Teardown.postman_collection.json -e ./unified_dashboard_tests/integration_tests/TestAutomation_Env.postman_environment.json -r htmlextra,cli --reporter-htmlextra-export ./output/reports/teardown.html --reporter-htmlextra-darkTheme  --timeout-request 120000

echo "\n"
echo "Execution finished"
echo "===============================================================================\n"
