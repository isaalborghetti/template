import pytest
from pytest_bdd import scenarios
from unified_dashboard_tests.page_objects.login_page import LoginPage
from unified_dashboard_tests.page_objects.home_page import HomePage
from unified_dashboard_tests.unified_dashboard_common.step_definitions.steps_then import *
from unified_dashboard_tests.unified_dashboard_common.step_definitions.steps_when import *


scenarios('../features/login.feature')


@pytest.fixture
def login_page(selenium, base_url, env_variables):
    return LoginPage(selenium, base_url, env_variables)


@pytest.fixture
def home_page(selenium, base_url, env_variables):
    return HomePage(selenium, base_url, env_variables)





